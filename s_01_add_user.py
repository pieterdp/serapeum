#!/usr/bin/env python3
from serapeum.lib.authentication import UserApi
from getpass import getpass
from serapeum.lib.exceptions import ItemNotFoundException, ItemExistsException
import argparse
from logging import getLogger


def _args():
    p = argparse.ArgumentParser()
    p.add_argument('--username', help='Username. Script will fail if user already exists.', required=True)
    p.add_argument('--role', action='append', help='Role. Repeat for extra roles.', required=True)
    return p.parse_args()


def _log(code, msg):
    logger = getLogger('flask.app')
    logger.error(msg)
    return code


def main():
    args = _args()
    user_api = UserApi()
    try:
        user_api.read(args.username)
    except ItemNotFoundException:
        pass
    except Exception as e:
        return _log(255, e)
    else:
        return _log(1, 'User {0} already exists.'.format(args.username))
    try:
        user_api.create({
            'username': args.username,
            'password': getpass(),
            'roles': args.role
        })
    except Exception as e:
        return _log(254, e)
    return 0


if __name__ == '__main__':
    exit(main())
