from serapeum.lib.db.models import Model
from serapeum.lib.db.models.creator import Creator
from serapeum.lib.db.models.location import Location
from serapeum.lib.db.models.subject import Subject
from serapeum.lib.db.models.series import Series
from serapeum.lib.db.models.material import Material
from serapeum.lib.db.models.book import Book
from serapeum.lib.db.models.user import User
from serapeum.lib.db.models.role import Role
from serapeum.lib.db import DB


if __name__ == '__main__':
    d = DB()
    Model.metadata.create_all(d.engine)
