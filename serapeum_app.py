from flask import Flask
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
from flask_babel import Babel, gettext
from jinja2 import ChoiceLoader, FileSystemLoader
from serapeum.admin.api.routes import library
from serapeum.public.search.routes import search
from serapeum.admin.gui.routes import admin_gui
from serapeum.public.gui.routes import public_gui
from serapeum.lib.authentication.routes import auth
from serapeum.lib.authentication import UserApi
from serapeum.lib.config import Config


application = Flask(__name__)
application.secret_key = Config().get('SECRET_KEY')
application.debug = Config().get('DEBUG')

csrf = CSRFProtect(application)

loader = ChoiceLoader([
    application.jinja_loader,
    FileSystemLoader('static/serapeum/dist')
])
application.jinja_loader = loader

login_manager = LoginManager()
login_manager.init_app(application)
login_manager.login_view = 'auth.login'
login_manager.login_message = gettext('U moet aangemeld zijn om deze pagina te kunnen bekijken.')

babel = Babel(
    application,
    default_locale=Config().get('BABEL_DEFAULT_LOCALE'),
    default_timezone=Config().get('BABEL_DEFAULT_TIMEZONE')
)


@login_manager.user_loader
def load_user(user_id):
    user_m = UserApi().by_id(user_id)
    if not user_m:
        return None
    return user_m


application.register_blueprint(library)
application.register_blueprint(search)
application.register_blueprint(admin_gui)
application.register_blueprint(public_gui)
application.register_blueprint(auth)

if __name__ == '__main__':
    application.run(debug=application.debug, host='0.0.0.0', threaded=False)
