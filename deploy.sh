#!/bin/bash

# Prepare deployment
CWD=$(pwd)
cd static/serapeum
./node_modules/@angular/cli/bin/ng build --base-href=/search/

cd "$CWD"

rm $HOME/Workspace/serapeum/serapeum.tar.bz2

mkdir -p /tmp/serapeum

rsync -avz serapeum/ /tmp/serapeum/serapeum/
mkdir -p /tmp/serapeum/static
mkdir -p /tmp/serapeum/static/serapeum/dist
rsync -avz static/css/ /tmp/serapeum/static/css/
rsync -avz static/startbootstrap-creative/ /tmp/serapeum/static/startbootstrap-creative/
rsync -avz static/serapeum/dist/serapeum/ /tmp/serapeum/static/serapeum/dist/serapeum/
rsync -avz templates/ /tmp/serapeum/templates/
rsync -avz supervisor/ /tmp/serapeum/supervisor/
rsync -avz config/ /tmp/serapeum/config/
rsync -avz bin/ /tmp/serapeum/bin/
rsync -avz serapeum_app.py /tmp/serapeum/
rsync -avz Pipfile /tmp/serapeum/
rsync -avz setup.py /tmp/serapeum/

tar -cjf $HOME/Workspace/serapeum/serapeum.tar.bz2 -C /tmp serapeum

rm -rf /tmp/serapeum

scp $HOME/Workspace/serapeum/serapeum.tar.bz2 pieter@serapeum.nidavellir.be:~/
