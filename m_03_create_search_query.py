#!/usr/bin/env python3

from serapeum.admin.api.books import LibraryBooks
from serapeum.lib.db import DB


def add_search_query():
    db = DB()
    api = LibraryBooks(db=db)
    for book in api.list(page=None):
        book.update_search()
        db.session.commit()


def main():
    add_search_query()
    return 0


if __name__ == '__main__':
    exit(main())

