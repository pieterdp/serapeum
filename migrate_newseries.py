#!/usr/bin/env python3

##
# Convert all series to the new format: instead of referencing the title, reference the ID
##
from serapeum.lib.legacy.mongo.book import MongoBookApi, MongoSeriesApi
from serapeum.lib.exceptions import ItemNotFoundException
from bson.errors import InvalidId


def main():
    b_a = MongoBookApi()
    s_a = MongoSeriesApi()
    for book in b_a.in_series():
        if book['dcterms:isPartOf'] == '' or book['dcterms:isPartOf'] == '':
            continue
        try:
            series = s_a.by_title_s(book['dcterms:isPartOf'])
        except ItemNotFoundException:
            try:
                series = s_a.read(book['dcterms:isPartOf'])
            except (ItemNotFoundException, InvalidId):
                series = s_a.create({
                    'name': book['dcterms:isPartOf']
                })
        b_a.update(book['_id'], {'series': series['_id']})
        s_a.add_book_to_series(series['_id'], book['_id'])
        print(b_a.read(book['_id']))
        print(s_a.read(series['_id']))


if __name__ == '__main__':
    exit(main())
