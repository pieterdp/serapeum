let getBook = (book_id, title_el, authors_el, location_el, series_el, subjects_el) => {

    fetch('/library/api/books/' + book_id)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                // Alert
            }
        })
        .then((data) => {
            title_el.textContent = data['data']['title'];
            authors_el.textContent = '';
            formatAuthors(authors_el, data['data']['creators']);
            location_el.textContent = '';
            formatLocation(location_el, data['data']['location']);
            if (data['data']['series'] !== undefined && data['data']['series'] !== null && data['data']['series'] !== '') {
                series_el.removeAttribute('hidden');
                let strong = series_el.querySelector('strong');
                while (strong.firstChild) {
                    strong.firstChild.remove();
                }
                formatSeries(strong, data['data']['series']['title']);
            } else {
                series_el.setAttribute('hidden', 'hidden');
            }
            if (data['data']['subjects'] !== undefined && data['data']['subjects'] !== null) {
                subjects_el.removeAttribute('hidden');
                let span = subjects_el.querySelector('span');
                if (span !== null && span !== undefined) {
                    while (span.firstChild) {
                        span.firstChild.remove();
                    }
                    formatSubjects(span, data['data']['subjects']);
                }
            } else {
                subjects_el.setAttribute('hidden', 'hidden');
            }
        });
};

let formatAuthors = (parent_el, authors) => {
    let ul = document.createElement('ul');
    ul.setAttribute('class', 'list-inline d-inline');
    for (let i = 0; i < authors.length; i++) {
        let author = authors[i];
        let li = document.createElement('li');
        li.setAttribute('class', 'list-inline-item');
        let a = document.createElement('a');
        a.setAttribute('href', '/books/?q=author=' + author['name']);
        a.textContent = author['name'];
        li.appendChild(a);
        ul.appendChild(li);
    }
    parent_el.appendChild(ul);
};

let formatSubjects = (parent_el, subjects) => {
    let ul = document.createElement('ul');
    ul.setAttribute('class', 'list-inline d-inline');
    for (let i = 0; i < subjects.length; i++) {
        let subject = subjects[i];
        let li = document.createElement('li');
        li.setAttribute('class', 'list-inline-item');
        let a = document.createElement('a');
        a.setAttribute('href', '/books/?q=' + subject['name']);
        a.textContent = subject['name'];
        let span = document.createElement('span');
        span.setAttribute('class', 'badge badge-light');
        span.appendChild(a);
        li.appendChild(span);
        ul.appendChild(li);
    }
    parent_el.appendChild(ul);
};

let formatLocation = (parent_el, location) => {
    let a = document.createElement('a');
    a.setAttribute('href', '/books/?q=location=' + location);
    a.textContent = location;
    parent_el.appendChild(a);
};

let formatSeries = (parent_el, series) => {
    let a = document.createElement('a');
    a.setAttribute('href', '/books/?q=series=' + series);
    a.textContent = series;
    parent_el.appendChild(a);
};

document.addEventListener('DOMContentLoaded', () => {
    let book_id_el = document.querySelector('#book_id');
    let title_el = document.querySelector('#title');
    let authors_el = document.querySelector('#authors');
    let location_el = document.querySelector('#location');
    let series_el = document.querySelector('#series');
    let subjects_el = document.querySelector('#subjects');
    getBook(book_id_el.textContent, title_el, authors_el, location_el, series_el, subjects_el);
});