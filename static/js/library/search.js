let my_page;
let total_pages;


let searchQueryValue = () => {
    let query_params = new URLSearchParams(window.location.search);
    return query_params.get('q');
};

let pageValue = () => {
    let query_params = new URLSearchParams(window.location.search);
    let p = query_params.get('p');
    if (p !== null && p !== undefined) {
        return parseInt(p);
    }
    return p;
};

let querySearch = (query, container_el) => {
    let query_a = query.split('=');
    let url;
    if (query_a.length === 1) {
        url = '/search/api/query/' + query;
    } else {
        url = '/search/api/' + query_a[0] + '/' + query_a[1];
    }
    url = url + '?page=' + my_page;
    fetch(url)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                // Alert
            }
        })
        .then((data) => {
            while (container_el.firstChild) {
                container_el.removeChild(container_el.firstChild);
            }
            if (data['data'] !== null && data['data'] !== undefined) {
                formatResults(container_el, data['data'], query);
            }
        });
};

let formatResults = (container_el, data, query_str) => {
    console.log('call_format_results');
    while (container_el.firstChild) {
        container_el.firstChild.remove();
    }
    let total = parseInt(data['total']);
    let results = data['results'];
    total_pages = parseInt(data['pages']);
    let page = parseInt(data['page']);
    for (let i = 0; i < results.length; i++) {
        let book = results[i];
        let col = document.createElement('div');
        col.setAttribute('class', 'col-sm-2 text-center');
        col.setAttribute('data-serapeum-id', book['id']);
        let it = document.createElement('div');
        it.setAttribute('class', 'service-box mt-5 mx-auto');
        let a = document.createElement('a');
        a.setAttribute('href', '/search/result/' + book['id']);
        let img = document.createElement('img');
        img.setAttribute('src', '/static/images/covers/yggdrassil-book-plate.png');
        img.setAttribute('class', 'img-thumbnail');
        img.setAttribute('style', 'max-height: 30vh');
        img.setAttribute('alt', 'Cover');
        a.appendChild(img);
        let h3 = document.createElement('h5');
        h3.setAttribute('class', 'mb-1');
        h3.textContent = book['title'];
        a.appendChild(h3);
        let ul = document.createElement('ul');
        ul.setAttribute('class', 'text-muted mb-0 list-inline');
        for (let j = 0; j < book['creators'].length; j++) {
            let li = document.createElement('li');
            li.setAttribute('class', 'list-inline-item');
            li.textContent = book['creators'][j]['name'];
            ul.appendChild(li);
        }
        a.appendChild(ul);
        let p = document.createElement('p');
        p.setAttribute('class', 'mb-0');
        p.textContent = book['location'];
        a.appendChild(p);
        it.appendChild(a);
        col.appendChild(it);
        container_el.appendChild(col);
    }
    let pagination_el = document.querySelector('#pages');
    if (my_page !== 1 && my_page <= total_pages) {
        // We're not on the first page
        let first_s = document.createElement('span');
        first_s.textContent = '| 1 |';
        first_s.setAttribute('class', 'mr-1');
        pagination_el.appendChild(first_s);
        let prev_a = document.createElement('a');
        prev_a.setAttribute('href', '/books/?q=' + query_str + '&p=' + (my_page - 1).toString());
        prev_a.textContent = '<<';
        prev_a.setAttribute('class', 'mr-1');
        pagination_el.appendChild(prev_a);
    }
    let current_page_s = document.createElement('span');
    current_page_s.textContent = '| ' + my_page + ' |';
    pagination_el.appendChild(current_page_s);
    if (my_page < total_pages) {
        // We're not on the last page
        let next_a = document.createElement('a');
        next_a.setAttribute('href', '/books/?q=' + query_str + '&p=' + (my_page + 1).toString());
        next_a.setAttribute('class', 'ml-1');
        next_a.textContent = '>>';
        pagination_el.appendChild(next_a);
        let last_s = document.createElement('span');
        last_s.textContent = '| ' + total_pages.toString() + ' |';
        last_s.setAttribute('class', 'ml-1');
        pagination_el.appendChild(last_s);
    }
};

/* Function that can be called by other scripts should they wish to reload the page */
let performSearch = () => {
    my_page = pageValue();
    if (my_page === null) {
        my_page = 1;
    }
    total_pages = 1;
    let query_el = document.querySelector('#q');
    let cont_el = document.querySelector('#library_cont');
    let query = searchQueryValue(query_el);
    if (query !== '' && query !== null) {
        my_page = pageValue();
        if (my_page === null) {
            my_page = 1;
        }
        total_pages = 1;
        querySearch(query, cont_el);
    }
};


document.addEventListener('DOMContentLoaded', () => {
    /* Keep current page + amount of pages global */
    my_page = pageValue();
    if (my_page === null) {
        my_page = 1;
    }
    total_pages = 1;
    let query_el = document.querySelector('#q');
    let search_b_el = document.querySelector('#search_btn');
    /* Search */
    performSearch();

    search_b_el.addEventListener('click', () => {
        let query = query_el.value;
        if (query === '' || query === null) {
            query = 'all';
        }
        window.location.search = '?q=' + query;
    });
});