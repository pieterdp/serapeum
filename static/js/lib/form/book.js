import {StatusModal} from "./components/status.js";
import {CSRFToken} from "./components/csrf_token.js";
import {Collector} from "./components/collector.js";
import {Validator} from "./components/validator.js";
import {Error} from "./components/error.js";
import {TitleCheck} from "./components/check/title.js";
import {Author} from "./components/author.js";
import {Subject} from "./components/subject.js";
import {Location} from "./components/location.js";
import {Series} from "./components/series.js";
import {ExistingItemComponent} from "./components/sources/data.js";

export class BookForm {

    constructor(
        form = document.querySelector('#bookForm'),
        edit = false
    ) {
        this.form = form;
        this.api = '/library/api/books';
        this.url = this.api;
        this.method = 'post';
        this.edit = edit;

        if (this.edit) {
            this.EditForm();
        }

        this.validator = new Validator(this.form);
        this.collector = new Collector(this.form);
        this.status = new StatusModal(this.form);
        this.csrf = new CSRFToken(this.form);
        this.titleCheck = new TitleCheck(this.form);
        this.error = new Error(this.form);

        /* Components */
        this.authorComponent = new Author(this.form, this.error);
        this.subjectComponent = new Subject(this.form, this.error);
        this.locationComponent = new Location(this.form, this.error);
        this.seriesComponent = new Series(this.form, this.error);

        /* Stuff */
        this.batchMode = false;


        /* Listeners */
        let cls = this;
        this.form.addEventListener('click', function (event) {
            if (event.target.getAttribute('id') !== 'submit') {
                return;
            }
            cls.validator.validate();
            if (cls.validator.hasValidationErrors) {
                return;
            }
            cls.collector.collect();
            cls.batchMode = cls.collector.data['batch'];
            if (!cls.batchMode) {
                cls.status.show();
            }
            cls.submitHandler(event);
        });

        this.form.addEventListener('change', function (event) {
            cls.validator.reset();
            cls.collector.reset();
            if (event.target.getAttribute('id') === 'title') {
                cls.titleCheck.check();
            }
        });
    }

    EditForm() {
        //<input type="hidden" id="_id" value=""/>
        let existingItemComponent = new ExistingItemComponent(
            '/library/api/books',
            this.form
        );
        this.url = [this.api, existingItemComponent.item_id].join('/');
        this.method = 'put';
        let cls = this;
        existingItemComponent.Data(
            function (data) {
                return data;
            },
            function (data) {
                Object.keys(data).forEach(function (key, index) {
                    let el = cls.form.querySelector('#' + key);
                    if (key === 'series') {
                        if (data[key] !== null) {
                            el.value = data[key]['title'];
                        }
                    } else if (key === 'creators') {
                        for (let i = 0; i < data[key].length; i++) {
                            cls.authorComponent.AddExisting(data[key][i]['name']);
                        }
                    } else if (key === 'subjects') {
                        for (let i = 0; i < data[key].length; i++) {
                            cls.subjectComponent.AddExisting(data[key][i]['name']);
                        }
                    } else if (key === 'location') {
                        cls.locationComponent.setLocation(data[key]);
                    } else {
                        if (el !== undefined && el !== null) {
                            el.value = data[key];
                        }
                    }
                });
                cls.form.querySelector('#material-container').removeAttribute('hidden');
            }
        );
        this.form.querySelector('#submit').textContent = 'Boek bewerken';
    }

    reset() {
        this.titleCheck.hide();
        this.validator.reset();
        this.collector.reset();
        if (this.edit) {
            this.EditForm();
        } else {
            this.form.querySelector('#title').value = null;
            this.authorComponent.reset();
            this.subjectComponent.reset();
        }
    }

    submitHandler(
        event
    ) {
        let book = {
            'title': this.collector.data['title'],
            /* Here is something fishy */
            'creator': this.authorComponent.collect(),
            'subjects': this.collector.data['subject'],
            'location': this.locationComponent.collect()
        }
        /* Material */
        if (this.edit) {
            book['material'] = this.collector.data['material'];
        }
        /* Series */
        this.series = null;
        if (this.collector.data['series'] !== null && this.collector.data['series'] !== undefined && this.collector.data['series'] !== '') {
            this.series = this.collector.data['series'];
            book['series'] = this.series;
        }

        let cls = this;

        fetch(cls.url, {
            method: cls.method,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': cls.csrf.token()
            },
            body: JSON.stringify(book)
        })
            .then(function (response) {
                /* Reset form */
                cls.error.reset();
                if (!cls.batchMode) {
                    cls.status.show();
                }

                /* Status */
                if (!response.ok) {
                    response.text().then(function (data) {
                        cls.status.failure(undefined, data);
                    });
                    return;
                }
                response.json().then(function (data) {
                    data = data['data'];
                    cls.status.success(function () {
                        let p = document.createElement('p');
                        p.setAttribute('class', 'text-success');
                        if (cls.edit) {
                            p.innerHTML = 'Boek <i>' + data['title'] + '</i> is aangepast.';
                        } else {
                            p.innerHTML = 'Boek <i>' + data['title'] + '</i> is toegevoegd aan de bibliotheek.';
                        }
                        return p;
                    });
                    if (cls.series === null) {
                        cls.subjectComponent.reset();
                        cls.authorComponent.reset();
                    } else {
                        cls.subjectComponent.refresh();
                        cls.authorComponent.refresh();
                    }
                    cls.seriesComponent.refresh();
                    cls.reset();
                });
            });
    }

}