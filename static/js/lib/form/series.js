import {StatusModal} from "./components/status.js";
import {CSRFToken} from "./components/csrf_token.js";
import {Collector} from "./components/collector.js";
import {Validator} from "./components/validator.js";
import {Error} from "./components/error.js";
import {TitleCheck} from "./components/check/title.js";
import {ExistingItemComponent} from "./components/sources/data.js";

export class SeriesForm {

    constructor(
        form = document.querySelector('#seriesForm'),
        edit = false
    ) {
        this.form = form;
        this.api = '/library/api/series';
        this.url = this.api;
        this.method = 'post';
        this.edit = edit;
        /* Not nice */
        this.materials = {
            default: 'Book',
            comics: 'Comic',
            journals: 'Journal'
        };
        this.db_materials = {};
        this.formType = this.getSeriesType();
        if (this.edit) {
            this.EditForm();
        } else {
            if (this.materials.hasOwnProperty(this.formType)) {
                this.form.querySelector('#material').value = this.materials[this.formType];
            } else {
                this.form.querySelector('#material').value = this.materials.default;
            }
        }

        this.validator = new Validator(this.form);
        this.collector = new Collector(this.form);
        this.status = new StatusModal(this.form);
        this.csrf = new CSRFToken(this.form);
        this.error = new Error(this.form);
        this.titleCheck = new TitleCheck(this.form, this.form.querySelector('#name'), '/library/api/series/');

        /* Listeners */
        let cls = this;
        this.form.addEventListener('click', function (event) {
            if (event.target.getAttribute('id') !== 'submit') {
                return;
            }
            cls.validator.validate();
            if (cls.validator.hasValidationErrors) {
                return;
            }
            cls.collector.collect();

            let data = {
                name: cls.collector.data['name'],
                material: cls.collector.data['material'],
                books: []
            };

            if (cls.collector.data['material'] !== null && cls.collector.data['material'] !== undefined && cls.collector.data['material'] !== '') {
                data['material'] = cls.collector.data['material'];
            }

            fetch(cls.url, {
                method: cls.method,
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': cls.csrf.token()
                },
                body: JSON.stringify(data)
            })
                .then(function (response) {
                    /* Reset form */
                    cls.error.reset();
                    cls.status.show();

                    /* Status */
                    if (!response.ok) {
                        response.text().then(function (data) {
                            cls.status.failure(undefined, data);
                        });
                        return;
                    }
                    response.json().then(function (data) {
                        data = data['data'];
                        cls.status.success(function () {
                            let p = document.createElement('p');
                            p.setAttribute('class', 'text-success');
                            if (cls.edit) {
                                p.innerHTML = 'Serie <i>' + data['title'] + '</i> is aangepast.';
                            } else {
                                p.innerHTML = 'Serie <i>' + data['title'] + '</i> is toegevoegd aan de bibliotheek.';
                            }
                            return p;
                        });
                        cls.reset();
                    });
                });
        });

        this.form.addEventListener('change', function (event) {
            cls.validator.reset();
            cls.collector.reset();
            if (event.target.getAttribute('id') === 'name') {
                cls.titleCheck.check();
            }
        });
    }

    EditForm() {
        let existingItemComponent = new ExistingItemComponent(
            '/library/api/series',
            this.form
        );
        this.url = [this.api, existingItemComponent.item_id].join('/');
        this.method = 'put';
        existingItemComponent.Data(
            function(data) {
                return {
                    'name': data['title'],
                    'material': data['material']
                };
            }
        );
        this.form.querySelector('#submit').textContent = 'Serie bewerken';
    }

    reset() {
        this.validator.reset();
        this.collector.reset();
        this.titleCheck.hide();
        if (this.edit) {
            this.EditForm();
        }
        if (!this.edit) {
            this.form.querySelector('#name').value = null;
        }
    }

    getSeriesType() {
        let url = new URL(window.location.href);
        let path_a = url.pathname.split('/');
        return path_a[path_a.indexOf('admin') + 1];
    }

}
