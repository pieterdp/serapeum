import {Error} from "./error.js";

export class Series {

    constructor(
        form = document.querySelector('#form'),
        error = undefined,
        input = document.querySelector('#series')
    ) {
        this.form = form;
        if (error === undefined || error === null) {
            this.error = new Error(this.form, 'series-error-container');
        } else {
            this.error = error;
        }
        this.input = input;

        this.DataList();
    }

    reset() {
        this.input.value = null;
    }

    refresh() {
        this.DataList();
    }

    DataList() {
        let datalist = this.form.querySelector('#series-datalist');
        if (datalist === undefined || datalist === null) {
            datalist = document.createElement('datalist');
            this.form.appendChild(datalist);
        }
        datalist.setAttribute('id', 'series-datalist');
        while(datalist.firstChild) {
            datalist.firstChild.remove();
        }
        this.input.setAttribute('list', 'series-datalist');
        let cls = this;

        fetch('/library/api/series?all=true&follow=false')
            .then(function(response) {
                if (!response.ok) {
                    response.text().then(function(data) {
                        cls.error.addError(data);
                    });
                    return;
                }
                response.json().then(function(data) {
                    data = data['data'];
                    for (let i = 0; i < data['results'].length; i++) {
                        let option = document.createElement('option');
                        option.setAttribute('value', data['results'][i]['title']);
                        datalist.appendChild(option);
                    }
                });
            });
    }

}