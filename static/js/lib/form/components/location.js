import {Error} from "./error.js";

export class Location {

    constructor(
        form = document.querySelector('#form'),
        error = undefined,
        container = document.querySelector('#location')
    ) {
        this.form = form;
        this.container = container;

        if (error === undefined || error === null) {
            this.error = new Error(this.form, 'location-error-container');
        } else {
            this.error = error;
        }

        let parts = this.container.querySelectorAll('select');
        this.room = parts[0];
        this.case = parts[1];
        this.plank = parts[2];

        this.current_room = this.room.value;
        this.current_case = this.case.value;
        this.current_plank = this.plank.value;

        this.Init();

        let cls = this;
        this.container.addEventListener('change', function (event) {
            cls.Update();
        });

    }

    collect() {
        this.current_room = this.room.value;
        this.current_case = this.case.value;
        this.current_plank = this.plank.value;
        return {
            room: this.current_room,
            rack: this.current_case,
            plank: this.current_plank
        };
    }

    setLocation(location) {
        let loc_a = location.split('.');
        let cls = this;
        this.Room(function() {
            cls.setRoom(loc_a[0]);
            cls.setCase(loc_a[1]);
            cls.setPlank(loc_a[2]);
        });
    }

    setRoom(room) {
        this.current_room = room;
        this.room.value = room;
    }

    setCase(book_case) {
        this.current_case = book_case;
        this.case.value = book_case;
    }

    setPlank(plank) {
        this.current_plank = plank;
        this.plank.value = plank;
    }

    Init() {
        this.Room();
    }

    Update(cb) {
        if (this.room.value !== this.current_room) {
            this.current_room = this.room.value;
            this.Case(cb);
        } else if (this.case.value !== this.current_case) {
            this.current_case = this.case.value;
            this.Plank(cb);
        }
    }

    Room(cb) {
        let cls = this;
        fetch('/library/api/locations/rooms?all=true')
            .then(function (response) {
                if (!response.ok) {
                    response.text().then(function (data) {
                        cls.error.addError(data);
                    });
                    return;
                }
                response.json().then(function (data) {
                    data = data['data'];
                    let rooms = [];
                    for (let i = 0; i < data['results'].length; i++) {
                        rooms.push(data['results'][i]['room']);
                    }
                    rooms.sort();
                    for (let i = 0; i < rooms.length; i++) {
                        let option = document.createElement('option');
                        option.setAttribute('value', rooms[i]);
                        option.textContent = rooms[i];
                        if (rooms[i] === cls.current_room) {
                            option.setAttribute('selected', 'selected');
                        }
                        cls.room.appendChild(option);
                    }
                    if (rooms.length > 0) {
                        cls.current_room = cls.room.value;
                    } else {
                        cls.room.setAttribute('disabled', 'disabled');
                    }
                    cls.Case(cb);
                });
            });
    }

    Case(cb) {
        if (this.current_room === null || this.current_room === undefined || this.current_room === '') {
            this.case.setAttribute('disabled', 'disabled');
            this.Plank();
            return;
        }
        let cls = this;
        fetch('/library/api/locations/rooms/' + this.current_room + '/cases?all=true')
            .then(function (response) {
                if (!response.ok) {
                    response.text().then(function (data) {
                        cls.error.addError(data);
                    });
                    return;
                }
                response.json().then(function (data) {
                    data = data['data'];
                    let cases = [];
                    for (let i = 0; i < data['results'].length; i++) {
                        cases.push(data['results'][i]['rack']);
                    }
                    cases.sort(function (a, b) {
                        return a - b;
                    });
                    while (cls.case.firstChild) {
                        cls.case.firstChild.remove();
                    }
                    for (let i = 0; i < cases.length; i++) {
                        let option = document.createElement('option');
                        option.setAttribute('value', cases[i]);
                        option.textContent = cases[i];
                        if (cases[i] === cls.current_case) {
                            option.setAttribute('selected', 'selected');
                        }
                        cls.case.appendChild(option);
                    }
                    if (cases.length > 0) {
                        cls.current_case = cls.case.value;
                    } else {
                        cls.case.setAttribute('disabled', 'disabled');
                    }
                    cls.Plank(cb);
                });
            });
    }

    Plank(cb) {
        if (this.current_room === null || this.current_room === undefined || this.current_case === null || this.current_case === undefined || this.current_room === '' || this.current_case === '') {
            this.plank.setAttribute('disabled', 'disabled');
            return;
        }
        let cls = this;
        fetch('/library/api/locations/rooms/' + this.current_room + '/cases/' + this.current_case + '/planks?all=true')
            .then(function (response) {
                if (!response.ok) {
                    response.text().then(function (data) {
                        cls.error.addError(data);
                    });
                    return;
                }
                response.json().then(function (data) {
                    data = data['data'];
                    let planks = [];
                    for (let i = 0; i < data['results'].length; i++) {
                        planks.push(data['results'][i]['plank']);
                    }
                    planks.sort(function (a, b) {
                        return a - b;
                    });
                    while (cls.plank.firstChild) {
                        cls.plank.firstChild.remove();
                    }
                    for (let i = 0; i < planks.length; i++) {
                        let option = document.createElement('option');
                        option.setAttribute('value', planks[i]);
                        option.textContent = planks[i];
                        if (planks[i] === cls.current_plank) {
                            option.setAttribute('selected', 'selected');
                        }
                        cls.plank.appendChild(option);
                    }
                    cls.current_plank = cls.plank.value;
                    if (cb !== undefined) {
                        cb();
                    }
                });
            });
    }

}