export class LocationSource {

    constructor(data_container_el, change_container_el) {
        this.data_container_el = data_container_el;
        if (change_container_el !== undefined) {
            this.change_container_el = change_container_el;
        } else {
            this.change_container_el = data_container_el;
        }
        this.data_room_el = this.data_container_el.querySelector('[data-serapeum-parameter=room]');
        this.data_case_el = this.data_container_el.querySelector('[data-serapeum-parameter=case]');
        this.data_plank_el = this.data_container_el.querySelector('[data-serapeum-parameter=plank]');
        this.change_room_el = this.change_container_el.querySelector('[data-serapeum-parameter=room]');
        this.change_case_el = this.change_container_el.querySelector('[data-serapeum-parameter=case]');
    }


    /**
     * Get rooms, cases and planks from the DB and fill in the form.
     * Cases are filled based on what a room contains.
     * Planks are filled based on what a case contains.
     */
    get() {
        let cls = this;

        /* Initial state */
        this.getRooms();

        /* Detect changes */
        this.change_room_el.addEventListener('change', function (event) {
            cls.getCases();
        });
        this.change_case_el.addEventListener('change', function (event) {
            cls.getPlanks();
        });
    }

    _empty_list(parent) {
        parent.querySelectorAll('option').forEach(function (el) {
            if (el.getAttribute('data-serapeum-remove') !== 'false') {
                el.remove();
            }
        });
    }

    _option(value, content) {
        if (content === undefined || content === null) {
            content = value;
        }
        let el = document.createElement('option');
        el.setAttribute('value', value);
        el.textContent = content;
        return el;
    }

    getRooms() {
        let cls = this;
        fetch('/library/api/locations/rooms')
            .then(function (response) {
                if (response.ok) {
                    response.json()
                        .then(function (data) {
                            let rooms = data['data']['results'];
                            cls._empty_list(cls.data_room_el);
                            for (let i = 0; i < rooms.length; i++) {
                                cls.data_room_el.appendChild(cls._option(rooms[i]['room']));
                            }
                            cls.getCases();
                        });
                } else {
                }
            });
    }

    getCases() {
        let cls = this;
        fetch('/library/api/locations/rooms/' + this.data_room_el.value + '/cases')
            .then(function (response) {
                if (response.ok) {
                    response.json()
                        .then(function (data) {
                            let cases = data['data']['results'];
                            cls._empty_list(cls.data_case_el);
                            for (let i = 0; i < cases.length; i++) {
                                cls.data_case_el.appendChild(cls._option(cases[i]['rack']));
                            }
                            cls.getPlanks();
                        });
                } else {
                }
            });
    }

    getPlanks() {
        let cls = this;
        fetch('/library/api/locations/rooms/' + this.data_room_el.value + '/cases/' + this.data_case_el.value + '/planks')
            .then(function (response) {
                if (response.ok) {
                    response.json()
                        .then(function (data) {
                            let planks = data['data']['results'];
                            cls._empty_list(cls.data_plank_el);
                            for (let i = 0; i < planks.length; i++) {
                                cls.data_plank_el.appendChild(cls._option(planks[i]['plank']));
                            }
                        });
                } else {
                }
            });
    }
}