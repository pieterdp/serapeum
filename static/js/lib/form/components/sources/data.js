import {Error} from "../error.js";
import {StatusModal} from "../status.js";

export class ExistingItemComponent {

    constructor(
        api_url,
        form = document.querySelector('#form'),
        item_id
    ) {
        this.form = form;
        this.api = api_url;
        if (item_id !== undefined) {
            this.item_id = item_id;
        } else {
            this.item_id = this.getItemId();
        }
        this.status = new StatusModal();
    }

    getItemId() {
        let url = new URL(window.location.href);
        let path_a = url.pathname.split('/');
        return path_a[path_a.length - 1];
    }

    Data(
        transform_cb,
        assign_cb
    ) {
        let cls = this;
        fetch([this.api, this.item_id].join('/'))
            .then(function(response) {
                if (!response.ok) {
                    response.text().then(function(data) {
                        cls.status.failure(undefined, data);
                    });
                    return;
                }
                response.json().then(function(data) {
                    if (transform_cb !== undefined) {
                        data = transform_cb(data['data']);
                    } else {
                        data = data['data'];
                    }
                    if (assign_cb !== undefined) {
                        assign_cb(data);
                    } else {
                        Object.keys(data).forEach(function(key, index) {
                            let el = cls.form.querySelector('#' + key);
                            if (el !== undefined && el !== null) {
                                el.value = data[key];
                            }
                        });
                    }
                });
            });
    }

}