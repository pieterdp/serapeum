export class Error {

    constructor(
        form = document.querySelector('#form'),
        id = 'form-error-container'
    ) {
        this.form = form;

        this.errors = [];

        this.container = document.createElement('div');
        this.container.setAttribute('id', id);
        this.form.appendChild(this.container);
    }


    reset() {
        while(this.container.firstChild) {
            this.container.firstChild.remove();
        }
        this.errors = [];
    }

    addError(error) {
        if (this.errors.indexOf(error) !== -1) {
            return;
        }
        this.errors.push(error);
        this.Error(error);
    }

    Error(error) {
        let alert = document.createElement('div');
        alert.setAttribute('class', 'alert alert-danger');
        alert.setAttribute('role', 'alert');
        alert.textContent = error;
        this.container.appendChild(alert);
    }

}