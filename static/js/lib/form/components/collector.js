export class Collector {

    constructor(
        form = document.querySelector('#form')
    ) {
        this.form = form;
        this.data = {};
    }

    collect() {
        let types = ['input', 'select', 'textarea'];
        let cls = this;
        for (let i = 0; i < types.length; i++) {
            this.form.querySelectorAll(types[i]).forEach(function(el, key) {
                let el_id = el.getAttribute('id');

                if (el.hasAttribute('data-serapeum-parameter') && el.getAttribute('data-serapeum-parameter') === 'multi-value') {
                    el_id = el_id.split('_')[0];
                }

                if (cls.data.hasOwnProperty(el_id)) {
                    if (!(cls.data[el_id] instanceof Array)) {
                        cls.data[el_id] = [cls.data[el_id]];
                    }
                    cls.data[el_id].push(cls.Value(el));
                } else {
                    cls.data[el_id] = cls.Value(el);
                }
            });
        }
    }

    Value(el) {
        if (el.getAttribute('type') !== 'checkbox' && el.getAttribute('type') !== 'radio') {
            return el.value;
        } else {
            return el.checked;
        }
    }

    reset() {
        this.data = {};
    }
}