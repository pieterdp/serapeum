export class TitleCheck {

    constructor(
        form = document.querySelector('#form'),
        title,
        upstream
    ) {
        this.form = form;
        if (title === undefined || title === null) {
            this.title_e = this.form.querySelector('#title');
        } else {
            this.title_e = title;
        }
        if (upstream !== undefined) {
            this.upstream = upstream;
        } else {
            this.upstream = '/search/api/title/';
        }
        this.element = this.ExistsElement();
    }

    ExistsElement() {
        //<div id="title_already" class="alert alert-warning mt-3" hidden="hidden">{{ _('Er bestaat al een werk met deze titel.') }}</div>
        let div = document.createElement('div');
        div.setAttribute('class', 'alert alert-warning mt-3');
        div.textContent = 'We hebben deze titel al. Bent u zeker dat u een nieuw exemplaar wil toevoegen?';
        return div;
    }

    check() {
        let cls = this;
        let url = this.upstream + this.title_e.value;
        if (url.includes('?')) {
            url = url + '&follow=false&name=true';
        } else {
            url = url + '?follow=false&name=true';
        }
        fetch(url)
            .then(function (response) {
                if (!response.ok) {
                    cls.hide();
                    return;
                }
                response.json().then(function (response) {
                    if (response['data'].hasOwnProperty('total')) {
                        if (response['data']['total'] > 0) {
                            cls.show();
                        } else {
                            cls.hide();
                        }
                    } else if(response['data'].hasOwnProperty('id')) {
                        cls.show();
                    }
                });
            });
    }

    show() {
        this.title_e.parentElement.appendChild(this.element);
    }

    hide() {
        this.element.remove();
    }

    reset() {
        this.hide();
    }

}