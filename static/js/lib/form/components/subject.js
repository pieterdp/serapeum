import {Error} from "./error.js";

/**
 * Subject component for a form
 * @param container_el
 * @constructor
 */
export class Subject {

    constructor(
        form = document.querySelector('#form'),
        error = undefined,
        container= document.querySelector('#subjects'),
        input = document.querySelector('#add_subject')
    ) {
        this.form = form;
        if (error === undefined || error === null) {
            this.error = new Error(this.form, 'subject-error-container');
        } else {
            this.error = error;
        }
        this.container = container;
        this.input = input;
        this.tags = {};
        this.inputs = {};
        this.subjects = [];
        this.ctr = 0;

        if (this.input === null || this.container === null) {
            // Form without subjects
            return;
        }

        this.DataList();

        let cls = this;

        this.input.addEventListener('change', function(event) {
            let subject = cls.input.value;
            cls.input.value = null;
            cls.Tag(subject);
            cls.Input(subject);
            cls.subjects.push(subject);
            cls.ctr++;
        });

        this.container.addEventListener('click', function(event) {
            if (!event.target.hasAttribute('data-serapeum-action') || event.target.getAttribute('data-serapeum-action') !== 'remove') {
                return;
            }
            let subject = event.target.textContent;
            if (cls.tags.hasOwnProperty(subject)) {
                cls.tags[subject].remove();
                delete cls.tags[subject];
            }
            if (cls.inputs.hasOwnProperty(subject)) {
                cls.inputs[subject].remove();
                delete cls.inputs[subject];
            }
            if (cls.subjects.indexOf(subject) !== -1) {
                cls.subjects.splice(cls.subjects.indexOf(subject), 1);
            }
        });
    }

    Tag(subject) {
        if (this.tags.hasOwnProperty(subject)) {
            return;
        }
        let tag = document.createElement('button');
        tag.setAttribute('type', 'button');
        tag.setAttribute('class', 'btn btn-primary btn-sm ml-1 mt-1');
        tag.setAttribute('data-serapeum-action', 'remove');
        tag.textContent = subject;
        this.tags[subject] = tag;
        this.container.appendChild(tag);
    }

    Input(subject) {
        if (this.inputs.hasOwnProperty(subject)) {
            return;
        }
        let input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('id', 'subject_' + this.ctr);
        input.setAttribute('data-serapeum-parameter', 'multi-value');
        input.setAttribute('value', subject);
        this.inputs[subject] = input;
        this.form.appendChild(input);
    }

    AddExisting(subject) {
        if (this.subjects.indexOf(subject) >= 0) {
            return;
        }
        this.subjects.push(subject);
        this.Tag(subject);
        this.Input(subject);
    }

    reset() {
        for (let i = 0; i < this.subjects.length; i++) {
            if (this.inputs.hasOwnProperty(this.subjects[i])) {
                this.inputs[this.subjects[i]].remove();
                delete this.inputs[this.subjects[i]];
            }
            if (this.tags.hasOwnProperty(this.subjects[i])) {
                this.tags[this.subjects[i]].remove();
                delete this.tags[this.subjects[i]];
            }
        }
        this.subjects = [];
        this.DataList();
    }

    refresh() {
        this.DataList();
    }

    /* Datalist */
    DataList() {
        let datalist = this.form.querySelector('#subjects-datalist');
        if (datalist === undefined || datalist === null) {
            datalist = document.createElement('datalist');
            this.form.appendChild(datalist);
        }
        datalist.setAttribute('id', 'subjects-datalist');
        while(datalist.firstChild) {
            datalist.firstChild.remove();
        }
        this.input.setAttribute('list', 'subjects-datalist');
        let cls = this;
        fetch('/library/api/subjects?all=true')
            .then(function(response) {
                if (!response.ok) {
                    response.text().then(function(data) {
                        cls.error.addError(data);
                    });
                    return;
                }
                response.json().then(function(data) {
                    data = data['data'];
                    for (let i = 0; i < data['results'].length; i++) {
                        let option = document.createElement('option');
                        option.setAttribute('value', data['results'][i]['name']);
                        datalist.appendChild(option);
                    }
                });
            })
    }

}
