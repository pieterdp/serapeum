export class Validator {

    constructor (
        form = document.querySelector('#form')
    ) {
        this.form = form;
        this.hasValidationErrors = false;
    }

    validate() {
        let elements = ['input', 'select', 'textarea'];
        let cls = this;
        for (let i = 0; i < elements.length; i++) {
            this.form.querySelectorAll(elements[i]).forEach(function(el, key) {
                if (el.hasAttribute('required')) {
                    if (el.value === '' || el.value === null || el.value === undefined) {
                        cls.validationFailed(el, key, i);
                        cls.hasValidationErrors = true;
                    }
                }
            })
        }
    }

    reset() {
        this.hasValidationErrors = false;
        this.form.querySelectorAll('.is-invalid').forEach(function(el) {
            let css_a = el.getAttribute('class').split(' ');
            css_a.splice(css_a.indexOf('is-invalid'), 1);
            el.setAttribute('class', css_a.join(' '));
        });
        this.form.querySelectorAll('[data-serapeum-invalid=invalid]').forEach(function(el) {
            el.remove();
        });
    }

    validationFailed(el, key, type_key) {
        let css_a = [];
        if (el.hasAttribute('class')) {
            css_a = el.getAttribute('class').split(' ');
        }
        if (css_a.indexOf('is-valid') !== -1) {
            css_a.splice(css_a.indexOf('is-valid'), 1);
        }
        if (css_a.indexOf('is-invalid') === -1) {
            css_a.push('is-invalid');
        }
        el.setAttribute('class', css_a.join(' '));
        //https://getbootstrap.com/docs/4.5/components/forms/#validation

        let invalid_id = 'invalid-' + type_key + '-' + key;
        let invalid = el.parentElement.querySelector('#' + invalid_id);
        if (invalid === null) {
            invalid = document.createElement('small');
        }
        invalid.setAttribute('id', invalid_id);
        invalid.setAttribute('class', 'text-danger mt-1 mb-1');
        invalid.setAttribute('data-serapeum-invalid', 'invalid');
        invalid.textContent = 'U moet iets invullen.';
        el.parentElement.appendChild(invalid);
    }

}