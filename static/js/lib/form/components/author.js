import {Error} from "./error.js";

export class Author {

    constructor(
        form = document.querySelector('#form'),
        error = undefined,
        container = document.querySelector('#authors'),
        input = document.querySelector('#add_author')
    ) {
        this.form = form;
        if (error === undefined || error === null) {
            this.error = new Error(this.form, 'subject-error-container');
        } else {
            this.error = error;
        }
        this.container = container;
        this.input = input;
        this.authors = {};
        this.inputs = {};
        this.creators = [];
        this.ctr = 0;

        this.DataList();

        let cls = this;
        this.input.addEventListener('change', function(event) {
            let author = cls.input.value;
            cls.input.value = null;

            cls.Button(author);
            cls.Input(author);

            cls.creators.push(author);
            cls.ctr++;
        });
        this.container.addEventListener('click', function(event) {
            if (!event.target.hasAttribute('data-serapeum-action') || event.target.getAttribute('data-serapeum-action') !== 'remove') {
                return;
            }
            let author = event.target.textContent;
            if (cls.authors.hasOwnProperty(author)) {
                cls.authors[author].remove();
                delete cls.authors[author];
            }
            if (cls.inputs.hasOwnProperty(author)) {
                cls.inputs[author].remove();
                delete cls.inputs[author];
            }
            if (cls.creators.indexOf(author) !== -1) {
                cls.creators.splice(cls.creators.indexOf(author), 1);
            }
        });
    }

    Button(author) {
        if (this.authors.hasOwnProperty(author)) {
            return;
        }
        let author_e = document.createElement('button');
        author_e.setAttribute('type', 'button');
        author_e.setAttribute('class', 'btn btn-primary btn-sm ml-1 mt-1');
        author_e.setAttribute('data-serapeum-action', 'remove');
        author_e.textContent = author;
        this.authors[author] = author_e;
        this.container.appendChild(author_e);
    }

    Input(author) {
        if (this.inputs.hasOwnProperty(author)) {
            return;
        }
        let input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('id', 'author_' + this.ctr);
        input.setAttribute('data-serapeum-parameter', 'multi-value');
        input.setAttribute('value', author);
        this.inputs[author] = input;
        this.form.appendChild(input);
    }

    AddExisting(author) {
        if (this.creators.indexOf(author) >= 0) {
            return;
        }
        this.creators.push(author);
        this.Button(author);
        this.Input(author);
    }

    reset() {
        for (let i = 0; i < this.creators.length; i++) {
            if (this.inputs.hasOwnProperty(this.creators[i])) {
                this.inputs[this.creators[i]].remove();
                delete this.inputs[this.creators[i]];
            }
            if (this.authors.hasOwnProperty(this.creators[i])) {
                this.authors[this.creators[i]].remove();
                delete this.authors[this.creators[i]];
            }
        }
        this.creators = [];
        this.DataList();
    }

    refresh() {
        this.DataList();
    }

    collect() {
        return this.creators;
    }

    DataList() {
        let datalist = this.form.querySelector('#authors-datalist');
        if (datalist === undefined || datalist === null) {
            datalist = document.createElement('datalist');
            this.form.appendChild(datalist);
        }
        datalist.setAttribute('id', 'authors-datalist');
        while(datalist.firstChild) {
            datalist.firstChild.remove();
        }
        this.container.querySelectorAll('input').forEach(function(el) {
            el.setAttribute('list', 'authors-datalist');
        });
        let cls = this;
        fetch('/library/api/authors?all=true')
            .then(function(response) {
                if (!response.ok) {
                    response.text().then(function(data) {
                        cls.error.addError(data);
                    });
                    return;
                }
                response.json().then(function(data) {
                    data = data['data'];
                    for (let i = 0; i < data['results'].length; i++) {
                        let option = document.createElement('option');
                        option.value = data['results'][i]['name'];
                        datalist.appendChild(option);
                    }
                });
            });
    }

}