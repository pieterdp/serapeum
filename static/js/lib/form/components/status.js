export class StatusModal {

    constructor(
        parent = document.querySelector('body')
    ) {
        this.parent = parent;
        this.modal_jq = null;
        this.body = null;
        this.footer = null;
        this.Modal();
    }

    show(cb) {
        while (this.body.firstChild) {
            this.body.firstChild.remove();
        }
        if (cb !== undefined) {
            this.body.appendChild(cb());
        } else {
            let p = document.createElement('p');
            p.setAttribute('class', 'text-primary');
            p.textContent = 'Verwerken';

            let spinner = document.createElement('div');
            spinner.setAttribute('class', 'spinner-border text-primary');
            spinner.setAttribute('role', 'status');
            let spinner_sr = document.createElement('span');
            spinner_sr.setAttribute('class', 'sr-only');
            spinner_sr.textContent = 'Aan het werken ...';
            spinner.appendChild(spinner_sr);

            /* Add to DOM */
            this.body.appendChild(p);
            this.body.appendChild(spinner);
        }
        this.modal_jq.modal('show');
    }

    success(cb) {
        while (this.body.firstChild) {
            this.body.firstChild.remove();
        }
        if (cb !== undefined) {
            this.body.appendChild(cb());
        } else {
            let p = document.createElement('p');
            p.setAttribute('class', 'text-success');
            p.textContent = 'Klaar!';

            /* Add to DOM */
            this.body.appendChild(p);
        }

        this.ModalCloseButton();
    }

    failure(cb, error) {
        while (this.body.firstChild) {
            this.body.firstChild.remove();
        }
        if (cb !== undefined) {
            this.body.appendChild(cb(error));
        } else {
            let p = document.createElement('p');
            p.setAttribute('class', 'text-danger');
            p.textContent = 'Er ging toch echt wel iets mis.';

            let error_d = document.createElement('div');
            error_d.setAttribute('class', 'alert alert-danger');
            error_d.setAttribute('role', 'alert');

            if (error !== undefined) {
                error_d.textContent = error;
            }

            /* Add to DOM */
            this.body.appendChild(p);
            if (error !== undefined) {
                this.body.appendChild(error_d);
            }
        }

        this.ModalCloseButton();
    }

    Modal() {
        let modal = document.createElement('div');
        modal.setAttribute('id', 'formStatusModal');
        modal.setAttribute('class', 'modal');
        modal.setAttribute('role', 'dialog');
        modal.setAttribute('tabindex', '-1');

        let dialog = document.createElement('div');
        dialog.setAttribute('class', 'modal-dialog');

        let content = document.createElement('div');
        content.setAttribute('class', 'modal-content');

        let body = document.createElement('div');
        body.setAttribute('class', 'modal-body text-center');

        /* Add to DOM */
        content.appendChild(body);
        dialog.appendChild(content);
        modal.appendChild(dialog);
        this.parent.appendChild(modal);
        this.modal_jq = $('#formStatusModal');
        this.body = body;
    }

    ModalCloseButton() {
        if (this.footer === null) {
            let footer = document.createElement('div');
            footer.setAttribute('class', 'modal-footer');

            let close = document.createElement('button');
            close.setAttribute('type', 'button');
            close.setAttribute('class', 'btn btn-secondary');
            close.setAttribute('data-dismiss', 'modal');
            close.textContent = 'Sluiten';

            /* Add to DOM */
            footer.appendChild(close);
            this.body.parentElement.appendChild(footer);

            this.footer = footer;
        }
    }

}