/**
 * A title component of a book form.
 * @param title_el
 * @param warn_el
 * @constructor
 */
export class Title {
    constructor(
        title_el,
        warn_el,
        upstream
    ) {
        this.element = title_el;
        this.warn_el = warn_el;
        if (upstream !== undefined) {
            this.upstream = upstream;
        } else {
            this.upstream = '/search/api/title/';
        }
    }

    /**
     * Check whether a title already exists in the DB
     */
    check() {
        //let warn_el = document.querySelector('#title_already');
        let cls = this;
        let title = this.element.value;
        fetch(this.upstream + title + '?name=false')
            .then((response) => {
                if (response.ok) {
                    response.json().then((data) => {
                        if (data['data']['total'] !== 0) {
                            if (data['data']['results'][0]['title'].toLowerCase() === title.toLowerCase()) {
                                cls.warn_el.removeAttribute('hidden');
                            }
                        }
                    });
                } else {
                    cls.warn_el.setAttribute('hidden', 'hidden');
                }
            });
    }

    /**
     * Remove the input and make ready for new input!
     */
    reset() {
        this.element.value = null;
        if (this.warn_el !== undefined) {
            this.warn_el.setAttribute('hidden', 'hidden');
        }
    }

    /**
     * Return the value
     */
    submit() {
        return this.element.value;
    }
}