export class CSRFToken {

    constructor(
        form = document.querySelector('#form')
    ) {
        this.form = form;
    }

    token() {
        return this.form.querySelector('#csrf_token').value;
    }

}