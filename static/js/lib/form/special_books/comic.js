import {BookForm} from "../book.js";

export class ComicForm extends BookForm {
    constructor(
        form = document.querySelector('#comicForm'),
        edit = false
    ) {
        super(form, edit);
        // material = Stripverhaal
    }

    submitHandler(event) {
        if (this.collector.data['series'] === null && this.collector.data['series'] === undefined && this.collector.data['series'] === '') {
            this.validator.hasValidationErrors = true;
            return;
        }

        let comic = {
            'title': this.collector.data['title'],
            'creator': this.authorComponent.collect(),
            'subjects': this.collector.data['subject'],
            'location': this.locationComponent.collect(),
            'series': this.collector.data['series'],
            'material': 'Stripverhaal'
        }
        let cls = this;

        fetch(cls.url, {
            method: cls.method,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': cls.csrf.token()
            },
            body: JSON.stringify(comic)
        })
            .then(function (response) {
                /* Reset form */
                cls.error.reset();
                if (!cls.batchMode) {
                    cls.status.show();
                }

                /* Status */
                if (!response.ok) {
                    response.text().then(function (data) {
                        cls.status.failure(undefined, data);
                    });
                    return;
                }
                response.json().then(function (data) {
                    data = data['data'];
                    cls.status.success(function () {
                        let p = document.createElement('p');
                        p.setAttribute('class', 'text-success');
                        if (cls.edit) {
                            p.innerHTML = 'Stripverhaal <i>' + data['title'] + '</i> is aangepast.';
                        } else {
                            p.innerHTML = 'Stripverhaal <i>' + data['title'] + '</i> is toegevoegd aan de bibliotheek.';
                        }
                        return p;
                    });
                    cls.authorComponent.refresh();
                    cls.seriesComponent.refresh();
                    cls.reset();
                });
            });
    }
}