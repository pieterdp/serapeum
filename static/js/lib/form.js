import {Title} from './form/components/title.js';
import {Author} from './form/components/author.js';
import {Subject} from './form/components/subject.js';
import {Location} from './form/components/location.js';

export class Form {

    constructor() {
        this.titleComponent = null;
        this.authorComponent = null;
        this.subjectComponent = null;
        this.locationComponent = null;
    }

    addTitleComponent(title_el, warn_el) {
        if (!(title_el instanceof Element)) {
            title_el = document.querySelector(title_el);
        }
        if (!(warn_el instanceof Element)) {
            warn_el = document.querySelector(warn_el);
        }
        this.titleComponent = new Title(title_el, warn_el);
    }

    addAuthorComponent(container_el) {
        if (!(container_el instanceof Element)) {
            container_el = document.querySelector(container_el);
        }
        this.authorComponent = new Author(container_el);
    }

    addSubjectComponent(container_el) {
        if (!(container_el instanceof Element)) {
            container_el = document.querySelector(container_el);
        }
        this.subjectComponent = new Subject(container_el);
    };

    addLocationComponent(container_el) {
        if (!(container_el instanceof Element)) {
            container_el = document.querySelector(container_el);
        }
        this.locationComponent = new Location(container_el);
    }

    /**
     * Create a simple input block
     * @param input_id
     * @param input_type
     * @param input_label
     * @param input_placeholder
     * @param required
     * @param warning_id
     * @param warning_text
     * @param input_value
     * @param tabindex
     * @param input_block_class
     * @returns {HTMLDivElement}
     */
    createSimpleInput(
        input_id,
        input_type,
        input_label,
        input_placeholder,
        required,
        warning_id,
        warning_text,
        input_value,
        tabindex,
        input_block_class
    ) {
        let div = document.createElement('div');
        if (input_block_class !== undefined) {
            div.setAttribute('class', 'form-group ' + input_block_class);
        } else {
            div.setAttribute('class', 'form-group');
        }

        let label = document.createElement('label');
        label.setAttribute('for', input_id);
        label.textContent = input_label;
        div.appendChild(label);

        let input = document.createElement('input');
        input.setAttribute('type', input_type);
        input.setAttribute('class', 'form-control');
        input.setAttribute('id', input_id);
        input.setAttribute('name', input_id);
        input.setAttribute('placeholder', input_placeholder);
        if (required !== undefined && required === true) {
            input.setAttribute('required', 'required');
        }
        if (input_value !== undefined) {
            input.setAttribute('value', input_value);
        }
        if (tabindex !== undefined) {
            input.setAttribute('tabindex', tabindex);
        }
        div.appendChild(input);

        if (warning_id !== undefined && warning_text !== undefined) {
            let warn = document.createElement('div');
            warn.setAttribute('id', warning_id);
            warn.setAttribute('class', 'alert alert-danger');
            warn.setAttribute('hidden', 'hidden');
            warn.textContent = warning_text;
            div.appendChild(warn);
        }
        return div;
    }

}