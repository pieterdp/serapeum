import {Error} from "../form/components/error.js";

export class SeriesList {

    constructor(
        container = document.querySelector('#series'),
        endpoint
    ) {
        this.container = container.querySelector('ul');
        if (this.container === undefined || this.container === null) {
            this.container = document.createElement('ul');
            container.appendChild(this.container);
        }
        this.container.setAttribute('class', 'list-group list-group-flush');
        this.container.setAttribute('style', 'width: 100%');
        this.error = new Error(container);
        if (endpoint === undefined) {
            endpoint = this.getSeriesType();
        }
        this.endpoint = endpoint;

        this.init();
    }

    init() {
        this.List();
    }

    refresh() {
        this.List();
    }

    List() {
        let cls = this;
        fetch('/library/api/' + this.endpoint + '?all=true&follow=false')
            .then(function(response) {
                if (!response.ok) {
                    response.text().then(function(data) {
                        cls.error.addError(data);
                    });
                    return;
                }
                response.json().then(function(data) {
                    while(cls.container.firstChild) {
                        cls.container.firstChild.remove();
                    }
                    cls.error.reset();
                    if (!data['data'].hasOwnProperty('results')) {
                        return;
                    }
                    for (let i = 0; i < data['data']['results'].length; i++) {
                        let series = data['data']['results'][i];
                        cls.Item(series['id'], series['title'], series['amount']);
                    }
                });
            });
    }

    Item(id, name, total) {
        let outer = document.createElement('li');
        outer.setAttribute('class', 'list-group-item d-flex justify-content-between align-items-center');
        let a = document.createElement('a');
        a.setAttribute('href', '/admin/series/edit/' + id);
        a.textContent = name;
        let badge = document.createElement('span');
        badge.setAttribute('class', 'badge badge-primary badge-pill');
        badge.textContent = total;
        outer.appendChild(a);
        outer.appendChild(badge);
        this.container.appendChild(outer);
    }

    getSeriesType() {
        let url = new URL(window.location.href);
        let path_a = url.pathname.split('/');
        return path_a[path_a.indexOf('admin') + 1];
    }

}