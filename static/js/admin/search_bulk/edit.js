let editForm = (resetFunction) => {
    let csrf_token = csrfToken();
    let selection = getSelection();

    /* Build the form */
    let author_add_el = document.querySelector('#button_author_add');
    let authors_el = document.querySelector('#authors');
    let title_el = document.querySelector('#title');
    let count = 0;
    author_add_el.addEventListener('click', () => {
        addAuthorInput(authors_el, count, undefined, undefined, undefined, author_add_el);
        count = count + 1;
    });
    let authors_datalist_el = document.querySelector('#authors_list');
    let series_datalist_el = document.querySelector('#series_list');
    let rooms_el = document.querySelector('#place');
    let cases_el = document.querySelector('#book_case');
    let planks_el = document.querySelector('#book_plank');
    getAuthors(authors_datalist_el);
    getSeries(series_datalist_el);
    getRooms(rooms_el, cases_el, planks_el);

    /* Show the form */
    let modal_el = $('#bulk_edit_modal');
    modal_el.modal('show');
    title_el.focus();

    /* Add submit event listeners */
    let submit_b = document.querySelector('#bulk_edit_submit');
    submit_b.addEventListener('click', () => {
        submitEditForm(selection, csrf_token, modal_el, resetFunction);
    });
};


let submitEditForm = (selection, csrf_token, modal_el, resetFunction) => {
    let title_el = document.querySelector('#title');
    let author_cont = document.querySelector('#authors');
    let location_cont = document.querySelector('#location');
    let series_el = document.querySelector('#series');
    let data = {};

    /* Collect data. Undefined or empty elements are not added (what is not present is not updated) */
    if (title_el.value !== undefined && title_el.value !== '') {
        data['title'] = title_el.value;
    }
    let authors = authorsSubmit(author_cont);
    if (authors.length !== 0 && authors[0] !== '' && authors[0] !== undefined) {
        data['creator'] = authors;
    }
    let location = locationSubmit(location_cont);
    if (location !== '' && location !== '.' && location !== '..') {
        data['location'] = location;
    }
    if (series_el.value !== undefined && series_el.value !== '') {
        data['series'] = series_el.value;
    }

    /* Hide the form (only 1 modal can be present) */
    modal_el.modal('hide');

    /* Show a status modal while the edits happen */
    let status_modal_el = $('#edit_status_modal');
    status_modal_el.modal('show');

    let edit_results_el = document.querySelector('#edit_results');
    edit_results_el.childNodes.forEach((node) => {
        node.remove();
    });

    /* Nice status animation */
    let busy_div = document.createElement('div');
    busy_div.setAttribute('class', 'text-center');
    let busy_icon = document.createElement('span');
    busy_icon.setAttribute('class', 'fa fa-circle-o-notch');
    busy_div.appendChild(busy_icon);
    let busy_p = document.createElement('p');
    busy_p.textContent = 'Bezig met het uitvoeren van de aanpassingen.';
    busy_div.appendChild(busy_p);
    edit_results_el.appendChild(busy_div);
    let dgs = 0;
    let interval_id = window.setInterval(() => {
        if (dgs === 360) {
            dgs = 0;
        }
        busy_icon.setAttribute('style', 'transform: rotate(' + dgs + 'deg)');
        dgs = dgs + 15;
    }, 40);

    let ul = document.createElement('ul');
    edit_results_el.appendChild(ul);

    /* Submit the updates */
    for (let i = 0; i < selection.length; i++) {
        let book_id = selection[i];
        fetch('/library/api/books/' + book_id, {
            method: 'PUT',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrf_token
            },
            redirect: 'follow',
            body: JSON.stringify(data)
        })
            .then((response) => {
                return response.json()
            })
            .then((api_data) => {
                let li = document.createElement('li');
                let a = document.createElement('a');
                a.setAttribute('href', '/search/result/' + api_data['data']['id']);
                a.textContent = api_data['data']['title'];
                li.appendChild(a);
                ul.appendChild(li);
                /* If this was the last one, remove the busy div and reset the form */
                if (i === selection.length - 1) {
                    window.clearInterval(interval_id);
                    busy_div.remove();

                    /* Say we're done */
                    let finished_div = document.createElement('div');
                    finished_div.setAttribute('class', 'text-center');
                    let finished_p = document.createElement('p');
                    finished_p.textContent = 'Aanpassingen uitgevoerd.';
                    let finished_icon = document.createElement('span');
                    finished_icon.setAttribute('class', 'fa fa-check-circle-o');
                    finished_div.appendChild(finished_icon);
                    finished_div.appendChild(finished_p);
                    edit_results_el.appendChild(finished_div);

                    /* Reset the edit form */
                    resetFunction();

                    /* Reload the search results */
                    performSearch();
                }
            });
    }
};