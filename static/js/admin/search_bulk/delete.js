let deleteForm = (resetFunction) => {
    let csrf_token = csrfToken();
    let selection = getSelection();

    /* Build the form */
    let delete_cont_d = document.querySelector('#delete_list');
    let ul = delete_cont_d.querySelector('ul'); // Remove whatever is left from the previous time.
    if (ul !== null && ul !== undefined) {
        ul.remove();
    }
    ul = document.createElement('ul');
    ul.setAttribute('class', 'list-unstyled mb-1');

    for (let i = 0; i < selection.length; i++) {
        let book_id = selection[i];
        fetch('/library/api/books/' + book_id, {
            method: 'GET',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrf_token
            },
            redirect: 'follow'
        })
            .then((response) => {
                return response.json()
            })
            .then((api_data) => {
                let title = api_data['data']['title'];
                let li = document.createElement('li');
                li.setAttribute('class', 'text-muted');
                li.textContent = title;
                ul.appendChild(li);
            });
    }

    delete_cont_d.appendChild(ul);

    /* Show the form */
    let modal_el = $('#bulk_delete_modal');
    modal_el.modal('show');

    /* Add submit event listeners */
    addSubmitButton(() => {
        submitDeleteForm(selection, csrf_token, modal_el, resetFunction, delete_cont_d);
    });
};


let addSubmitButton = (deleteListener) => {
    let submit_b = document.createElement('button');
    submit_b.setAttribute('class', 'btn btn-primary');
    submit_b.setAttribute('id', 'bulk_delete_submit');
    submit_b.textContent = 'Verwijderen';
    submit_b.addEventListener('click', deleteListener);
    let cont_el = document.querySelector('#bulk_delete_submit_container');
    cont_el.insertBefore(submit_b, cont_el.firstChild);
};


let submitDeleteForm = (selection, csrf_token, modal_el, resetFunction, container_el) => {
    /* Remove submit button to prevent double submits */
    document.querySelector('#bulk_delete_submit').remove();

    /* Nice status animation */
    let busy_div = document.createElement('div');
    busy_div.setAttribute('class', 'text-center');
    let busy_icon = document.createElement('span');
    busy_icon.setAttribute('class', 'fa fa-circle-o-notch');
    busy_div.appendChild(busy_icon);
    let busy_p = document.createElement('p');
    busy_p.textContent = 'Bezig met verwijderen.';
    busy_div.appendChild(busy_p);
    container_el.appendChild(busy_div);
    let dgs = 0;
    let interval_id = window.setInterval(() => {
        if (dgs === 360) {
            dgs = 0;
        }
        busy_icon.setAttribute('style', 'transform: rotate(' + dgs + 'deg)');
        dgs = dgs + 15;
    }, 40);

    /* Process the deletion */
    for (let i = 0; i < selection.length; i++) {
        let book_id = selection[i];
        fetch('/library/api/books/' + book_id, {
            method: 'DELETE',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrf_token
            },
            redirect: 'follow'
        })
            .then((response) => {
                return response.json()
            })
            .then((api_data) => {
                /* If this was the last one, remove the busy div and reset the form */
                if (i === selection.length - 1) {
                    window.clearInterval(interval_id);
                    busy_div.remove();

                    /* Say we're done */
                    let finished_div = document.createElement('div');
                    finished_div.setAttribute('class', 'text-center');
                    let finished_p = document.createElement('p');
                    finished_p.textContent = 'Verwijdering voltooid.';
                    let finished_icon = document.createElement('span');
                    finished_icon.setAttribute('class', 'fa fa-check-circle-o');
                    finished_div.appendChild(finished_icon);
                    finished_div.appendChild(finished_p);
                    container_el.appendChild(finished_div);

                    /* Reset the edit form */
                    resetFunction();

                    /* Reload the search results */
                    performSearch();
                }
            });
    }
};