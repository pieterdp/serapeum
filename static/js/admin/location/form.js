import {StatusModal} from '../../lib/form/components/status.js';
import {Validator} from '../../lib/form/components/validator.js';
import {Collector} from '../../lib/form/components/collector.js';
import {CSRFToken} from '../../lib/form/components/csrf_token.js';

export class LocationEditForm {

    constructor(
        form_e = document.querySelector('#locationForm')
    ) {
        this.form = form_e;
        this.validator = new Validator(this.form);
        this.collector = new Collector(this.form);
        this.csrf = new CSRFToken(this.form);
        this.submit = this.form.querySelector('#submit');
        this.datalists = {
            'room': this.form.querySelector('#rooms'),
            'case': this.form.querySelector('#cases'),
            'plank': this.form.querySelector('#planks')
        };
        this.inputs = {
            'room': this.form.querySelector('#room'),
            'case': this.form.querySelector('#case'),
            'plank': this.form.querySelector('#plank')
        };
        this.parts = ['room', 'case', 'plank'];
        this.url_parts = {
            'room': 'rooms',
            'case': 'cases',
            'plank': 'planks'
        };
        this.status = new StatusModal(
            this.form
        );
        let cls = this;

        /* Init state */
        this.getDataList();
        /* Load data lists and enable inputs */
        this.form.addEventListener('input', function (event) {
            let el_id = event.target.getAttribute('id');
            /* Enable the subsequent input or disable it if no value given */
            if (cls.parts.indexOf(el_id) + 1 < cls.parts.length) {
                if (event.target.value !== '' && event.target.value !== null) {
                    let next_part = cls.parts[cls.parts.indexOf(el_id) + 1];
                    cls.inputs[next_part].removeAttribute('disabled');
                    if (cls.inputs[next_part].value !== '' && cls.inputs[next_part].value !== null) {
                        /* Enable submit button */
                        cls.submit.removeAttribute('disabled');
                    }
                } else {
                    for (let i = cls.parts.indexOf(el_id) + 1; i < cls.parts.length; i++) {
                        let next_part = cls.parts[i];
                        cls.inputs[next_part].setAttribute('disabled', 'disabled');
                    }
                    /* Disable submit button */
                    cls.submit.setAttribute('disabled', 'disabled');
                }
            } else {
                /* Enable submit button */
                cls.submit.removeAttribute('disabled');
            }
            /* Load the corresponding datalist */
            cls.getDataList(cls.parts.indexOf(el_id));
            cls.validator.reset();
            cls.collector.reset();
        });
        /* Submit button */
        this.form.addEventListener('click', function (event) {
            if (event.target.getAttribute('id') !== 'submit') {
                return;
            }
            cls.validator.validate();
            if (cls.validator.hasValidationErrors) {
                return;
            }
            cls.status.show();
            cls.collector.collect();
            let url = '/library/api/locations';
            fetch(url, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': cls.csrf.token()
                },
                body: JSON.stringify({
                    code: cls.collector.data['room'],
                    b_case: cls.collector.data['case'],
                    plank: cls.collector.data['plank']
                })
            })
                .then(function(response) {
                    if (!response.ok) {
                        response.text().then(function(data) {
                            try {
                                let msg = JSON.parse(data)['msg'];
                                cls.status.failure(undefined, msg);
                            }
                            catch (e) {
                                cls.status.failure(undefined, data);
                            }
                        });
                        return;
                    }
                    response.json().then(function(data) {
                        cls.status.success();
                        cls.getDataList();
                    });
                });
        });
    }

    getDataList(pos = 0) {
        let part = this.parts[pos];
        let datalist = this.datalists[part];
        let url;
        if (pos > 0) {
            url = '/library/api/locations/';
            for (let i = 0; i <= pos; i++) {
                url = url + this.url_parts[this.parts[i]];
                if (i === pos) {
                    url = url + '?all=true';
                } else {
                    url = url + '/' + this.inputs[this.parts[i]].value + '/';
                }
            }
        } else {
            url = '/library/api/locations/' + this.url_parts[part] + '?all=true';
        }
        fetch(url)
            .then(function (response) {
                if (!response.ok) {
                    // Fail silently
                    return;
                }
                response.json().then(function (response) {
                    let data = response['data']['results'];
                    for (let i = 0; i < data.length; i++) {
                        let option = document.createElement('option');
                        option.value = data[i][part];
                        datalist.appendChild(option);
                    }
                });
            })
    }

}