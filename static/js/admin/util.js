let csrfToken = () => {
    return document.querySelector('#csrf_token').value;
};

let getAuthors = (datalist_el) => {
    fetch('/library/api/authors?all=true').then(
        function (response) {
            if (response.ok) {
                response.json().then((data) => {
                    datalist_el.querySelectorAll('option').forEach((el) => {
                        el.remove();
                    });
                    let authors = data['data']['results'];
                    //let datalist_el = document.querySelector('#authors_list');
                    for (let i = 0; i < authors.length; i++) {
                        let datalist_item_el = document.createElement('option');
                        datalist_item_el.setAttribute('value', authors[i]['name']);
                        datalist_el.appendChild(datalist_item_el);
                    }
                });
            } else {
                response.json().then((data) => {
                    let msg = data['msg'];
                    console.log(msg);
                })
            }
        }
    );
};

let getSeries = (datalist_el) => {
    fetch('/library/api/series?all=true&follow=false').then(
        function (response) {
            if (response.ok) {
                response.json().then((data) => {
                    datalist_el.querySelectorAll('option').forEach((el) => {
                        el.remove();
                    });
                    let series = data['data']['results'];
                    //let datalist_el = document.querySelector('#series_list');
                    for (let i = 0; i < series.length; i++) {
                        let datalist_item_el = document.createElement('option');
                        datalist_item_el.setAttribute('value', series[i]['dc:title']);
                        datalist_el.appendChild(datalist_item_el);
                    }
                });
            } else {
                response.json().then((data) => {
                    let msg = data['msg'];
                    console.log(msg);
                })
            }
        }
    );
};

let getRooms = (rooms_el, cases_el, planks_el, onFinished) => {
    fetch('/search/api/location/rooms?all=true').then(
        function (response) {
            if (response.ok) {
                response.json().then((data) => {
                    let rooms = data['data'];
                    //let rooms_el = document.querySelector('#place');
                    rooms_el.querySelectorAll('option').forEach((el) => {
                        if (el.getAttribute('data-serapeum-remove') !== 'false') {
                            el.remove();
                        }
                    });
                    rooms_el.addEventListener('change', () => {
                        getCases(rooms_el.value, cases_el, planks_el);
                    });
                    for (let i = 0; i < rooms.length; i++) {
                        let option_el = document.createElement('option');
                        option_el.setAttribute('value', rooms[i]);
                        option_el.textContent = rooms[i];
                        rooms_el.appendChild(option_el);
                    }
                    getCases(rooms_el.value, cases_el, planks_el, onFinished);
                });
            }
        }
    )
};

let getCases = (room, cases_el, planks_el, onFinished) => {
    /*if (room === '' || room === undefined) {
        return;
    }*/
    fetch('/search/api/location/rooms/' + room + '/cases?all=true').then(
        function (response) {
            if (response.ok) {
                response.json().then((data) => {
                    let cases = data['data'];
                    //let cases_el = document.querySelector('#book_case');
                    cases_el.querySelectorAll('option').forEach((el) => {
                        if (el.getAttribute('data-serapeum-remove') !== 'false') {
                            el.remove();
                        }
                    });
                    cases_el.addEventListener('change', () => {
                        getPlanks(room, cases_el.value, planks_el);
                    });
                    for (let i = 0; i < cases.length; i++) {
                        let option_el = document.createElement('option');
                        option_el.setAttribute('value', cases[i]);
                        option_el.textContent = cases[i];
                        cases_el.appendChild(option_el);
                    }
                    getPlanks(room, cases_el.value, planks_el, onFinished);
                });
            }
        }
    );
};

let getPlanks = (room, book_case, planks_el, onFinished) => {
    /*if (room === '' || room === undefined || book_case === '' || book_case === undefined) {
        return;
    }*/
    fetch('/search/api/location/rooms/' + room + '/cases/' + book_case + '/planks?all=true').then(
        function (response) {
            if (response.ok) {
                response.json().then((data) => {
                    let planks = data['data'];
                    //let planks_el = document.querySelector('#book_plank');
                    planks_el.querySelectorAll('option').forEach((el) => {
                        if (el.getAttribute('data-serapeum-remove') !== 'false') {
                            el.remove();
                        }
                    });
                    for (let i = 0; i < planks.length; i++) {
                        let option_el = document.createElement('option');
                        option_el.setAttribute('value', planks[i]);
                        option_el.textContent = planks[i];
                        planks_el.appendChild(option_el);
                    }
                    if (onFinished !== undefined && onFinished !== null) {
                        onFinished();
                    }
                });
            }
        }
    );
};

let getSubjects = (datalist_el) => {
    fetch('/library/api/subjects?all=true').then((response) => {
        return response.json();
    }).then((api_data) => {
        datalist_el.querySelectorAll('option').forEach((el) => {
            el.remove();
        });
        let subjects = api_data['data']['results'];
        //let datalist_el = document.querySelector('#authors_list');
        for (let i = 0; i < subjects.length; i++) {
            let datalist_item_el = document.createElement('option');
            datalist_item_el.setAttribute('value', subjects[i]['name']);
            datalist_el.appendChild(datalist_item_el);
        }
    });
};

