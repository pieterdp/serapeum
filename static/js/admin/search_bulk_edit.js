let addSelectBox = () => {
    let cont_el = document.querySelector('#library_cont');

    cont_el.querySelectorAll('.col-sm-2').forEach((element, index) => {
        let div = document.createElement('div');
        div.setAttribute('class', 'mt-1');
        div.setAttribute('data-serapeum-type', 'edit-item');
        let form = document.createElement('form');
        let checkbox_d = document.createElement('div');
        checkbox_d.setAttribute('class', 'form-group form-check');
        let checkbox_i = document.createElement('input');
        checkbox_i.setAttribute('type', 'checkbox');
        checkbox_i.setAttribute('class', 'form-check-input');
        checkbox_i.setAttribute('id', 'check-item-' + index);
        let checkbox_l = document.createElement('label');
        checkbox_l.setAttribute('for', 'check-item-' + index);
        checkbox_l.setAttribute('class', 'sr-only');
        checkbox_l.textContent = 'Selecteer voor bulkaanpassing.';
        checkbox_d.appendChild(checkbox_i);
        checkbox_d.appendChild(checkbox_l);
        form.appendChild(checkbox_d);
        div.appendChild(form);
        element.insertBefore(div, element.firstChild);
    });
};

let removeSelectBox = () => {
    document.querySelectorAll('[data-serapeum-type=edit-item]').forEach((element) => {
        element.remove();
    });
};

let addUnselectOption = (parent_el) => {
    let action_b = document.createElement('button');
    //class="dropdown-item btn disabled" type="button" id="bulk_edit"
    action_b.setAttribute('type', 'button');
    action_b.setAttribute('id', 'bulk_cancel');
    action_b.setAttribute('class', 'dropdown-item btn');
    let span = document.createElement('span');
    span.setAttribute('class', 'small');
    span.textContent = 'Annuleren';
    action_b.appendChild(span);
    parent_el.appendChild(action_b);
    return action_b;
};

let removeUnselectOption = (parent_el) => {
    parent_el.querySelector('#bulk_cancel').remove();
};

let getSelection = () => {
    /* Get a list of all selected items */
    let selected = [];
    document.querySelectorAll('[data-serapeum-type="edit-item"]').forEach((element) => {
        if (element.querySelector('input').checked) {
            selected.push(element.parentElement.getAttribute('data-serapeum-id'));
        }
    });
    return selected;
};

document.addEventListener('DOMContentLoaded', () => {
    let select_b = document.querySelector('#bulk_select');
    let edit_b = document.querySelector('#bulk_edit');
    let delete_b = document.querySelector('#bulk_remove');
    let tag_b = document.querySelector('#bulk_tag');
    let select_all_b = document.querySelector('#bulk_select_all');
    let action_d = document.querySelector('#bulk_actions');

    let resetEditFormListener = () => {
        removeSelectBox();
        select_b.addEventListener('click', editListener);
        enableActions([select_b]);
        disableActions([select_all_b, edit_b, delete_b, tag_b]);
        removeUnselectOption(action_d);
    };

    let editListener = () => {
        addSelectBox();
        /* Prevent selection boxes appearing twice */
        select_b.removeEventListener('click', editListener);
        /* Enable the actions and remove the select menu item */
        enableActions([select_all_b, edit_b, delete_b, tag_b]);
        disableActions([select_b]);
        let unselect_b = addUnselectOption(action_d);
        unselect_b.addEventListener('click', resetEditFormListener);
    };

    select_b.addEventListener('click', editListener);
    edit_b.addEventListener('click', () => {
        editForm(resetEditFormListener);
    });
    delete_b.addEventListener('click', () => {
        deleteForm(resetEditFormListener);
    });
    tag_b.addEventListener('click', () => {
        tagForm(resetEditFormListener);
    });
    select_all_b.addEventListener('click', () => {
        document.querySelectorAll('[data-serapeum-type=edit-item]').forEach((element) => {
            element.querySelector('input').setAttribute('checked', 'checked');
        });
    });
});