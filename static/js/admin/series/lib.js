/**
 * Create an overview (list of items + amount of child items - e.g. list of series and amount of books per series)
 * @param url
 * @param edit_link - base of the edit link
 * @param cont_e
 */
let overview = function (
    url,
    edit_link,
    cont_e
) {
    if (!(cont_e instanceof Element)) {
        cont_e = document.querySelector(cont_e);
    }
    fetch(url, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow'
    })
        .then((response) => {
            return response.json()
        })
        .then((api_data) => {
            let ul = document.createElement('ul');
            ul.setAttribute('class', 'list-group list-group-flush');
            ul.setAttribute('id', 'list-of-series');
            cont_e.appendChild(ul);
            for (let i = 0; i < api_data['data']['results'].length; i++) {
                let result = api_data['data']['results'][i];
                let li = document.createElement('li');
                li.setAttribute('class', 'list-group-item d-flex justify-content-between align-items-center');
                let a = document.createElement('a');
                a.setAttribute('href', edit_link + result['id']);
                if (result.hasOwnProperty('title')) {
                    a.textContent = result['title'];
                } else if (result.hasOwnProperty('name')) {
                    a.textContent = result['name'];
                } else {
                    a.textContent = result['id'];
                }
                li.appendChild(a);
                if (result.hasOwnProperty('amount')) {
                    let span = document.createElement('span');
                    span.setAttribute('class', 'badge-primary badge-pill');
                    span.textContent = result['amount'];
                    li.appendChild(span);
                }
                ul.appendChild(li);
            }
        });
};