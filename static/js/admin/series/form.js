import {SeriesForm} from "../../lib/form/series.js";

document.addEventListener('DOMContentLoaded', function() {
    let url = new URL(window.location.href);
    let url_a = url.pathname.split('/');
    if (url_a.indexOf('edit') === -1) {
        let form = new SeriesForm(document.querySelector('#seriesForm'));
    } else {
        let form = new SeriesForm(document.querySelector('#seriesForm'), true);
    }
});