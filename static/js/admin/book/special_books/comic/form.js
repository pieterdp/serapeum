import {ComicForm} from "../../../../lib/form/special_books/comic.js";

document.addEventListener('DOMContentLoaded', function() {
    let url = new URL(window.location.href);
    let url_a = url.pathname.split('/');
    if (url_a.indexOf('edit') === -1) {
        let form = new ComicForm(document.querySelector('#comicForm'));
    } else {
        let form = new ComicForm(document.querySelector('#comicForm'), true);
    }
});