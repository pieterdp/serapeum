import {BookForm} from "../../lib/form/book.js";

document.addEventListener('DOMContentLoaded', function() {
    let url = new URL(window.location.href);
    let url_a = url.pathname.split('/');
    if (url_a.indexOf('edit') === -1) {
        let form = new BookForm(document.querySelector('#bookForm'));
    } else {
        let form = new BookForm(document.querySelector('#bookForm'), true);
    }
});