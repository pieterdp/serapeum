let enableActions = (action_bs) => {
    for (let i = 0; i < action_bs.length; i++) {
        let action_b = action_bs[i];
        if (action_b !== undefined) {
            action_b.setAttribute('class', 'dropdown-item btn');
        }
    }
};

let disableActions = (action_bs) => {
    for (let i = 0; i < action_bs.length; i++) {
        let action_b = action_bs[i];
        if (action_b !== undefined) {
            action_b.setAttribute('class', 'dropdown-item btn disabled');
        }
    }
};