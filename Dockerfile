FROM python:3

WORKDIR /code

COPY ./ /code/

RUN pip install --no-cache-dir -r requirements.txt

COPY config/settings.json.example config/settings.json

CMD ["python", "./serapeum_app.py"]