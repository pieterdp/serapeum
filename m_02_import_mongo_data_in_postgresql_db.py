#!/usr/bin/env python3

from serapeum.lib.legacy.mongo.book import MongoBookApi
from serapeum.admin.api.books import LibraryBooks, ItemNotFoundException


def migrate_books():
    required = ['creator', 'title', 'location']
    valid = ['series', 'subjects', 'material']
    b = LibraryBooks()
    for book in MongoBookApi().list():
        book_p = {
            'creator': book['dc:creator'],
            'title': book['dc:title'],
            'location': book['dc:source']
        }
        if 'dc:subject' in book:
            book_p['subjects'] = book['dc:subject']
        if '_series_t' in book:
            book_p['series'] = book['_series_t']
        if 'dc:type' in book:
            book_p['material'] = book['dc:type'].title()
        print(book)
        try:
            book_o = b.by_title(book_p['title'])
        except ItemNotFoundException:
            print('created')
            book_o = b.create(book_p)
        print(book_o)


def main():
    migrate_books()
    return 0


if __name__ == '__main__':
    exit(main())

