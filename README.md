**Serapeum** is a smallscale library application for those of you that have lots of books and want to give them a minimum of organization. It is designed for easy adding of new books, and for retrieving them by author and title. Gone are the days where you stand in a store wondering whether you already have the fourth book of the trilogy in five parts that is the Hitchhikers Guide to the Galaxy.

It is completely written in Python, with a javascript-based UI using a theme from  [startbootstrap.com](https://startbootstrap.com/themes/creative/) (MIT licensed), using Mongo as a back-end. The entire application (apart from the UI elements taken from the theme) is available under the GPL v3 licence.

# Usage

Serapeum is a web application, so all interactions use the browser. The application consists of two parts: the search interface where you look for and find books, and the entry interface where you add new books. Everything is available from the menu.

# Installation

## Prerequisites

* A Mongo database server with [authentication enabled](https://docs.mongodb.com/manual/tutorial/enable-authentication/).
* An application server with Python 3.6 and Pip.

## Step-by-step process

_An Ansible playbook is available in the `deployment` directory that will run all the steps described here, apart from adding an admin user._

1. Create a user on your database server and give it permissions to read and write the _serapeum_ database.
2. Create an application user on your application server to run the application. I use _serapeum_ with home directory _/var/opt/app/serapeum_, but any will do. Create this user as a system user without shell to increase security.
3. Change to the user by executing `su serapeum -s /bin/bash` and clone the application.
    ```bash
    git clone /var/opt/app/serapeum/application
    ```
4. Create a virtual environment to install your dependencies.
    ```bash
    python3 -m venv /var/opt/app/serapeum/venv
    ```
5. Once inside the virtual environment, install all required packages (from _requirements.txt_).
    ```bash
    . /var/opt/app/serapeum/venv/bin/activate
    pip install -r /var/opt/app/serapeum/application/requirements.txt
    ```
6. As soon as the installation has finished, create a _settings.json_ file in _config_ (_/var/opt/app/serapeum/application/config/settings.json_). _settings.json.example_ is provided as an example and has all parameters that are necessary.
7. The recommended way to run Serapeum is via Gunicorn (included in the requirements) wrapped in a [SystemD](https://www.freedesktop.org/wiki/Software/systemd/) unit file.
    ```
    [Unit]
    Description = Serapeum Library
    After = network.target
    
    [Service]
    PermissionsStartOnly = true
    RuntimeDirectory = serapeum
    PIDFile = /run/serapeum/serapeum.pid
    User = serapeum
    Group = serapeum
    WorkingDirectory = /var/opt/app/serapeum/application
    ExecStart = /var/opt/app/serapeum/venv/bin/gunicorn serapeum_app:application -b 127.0.0.1:5000 --pid /run/serapeum/serapeum.pid
    ExecReload = /bin/kill -s HUP $MAINPID
    ExecStop = /bin/kill -s TERM $MAINPID
    
    [Install]
    WantedBy = multi-user.target
    ```
8. [Enable the unit file and start the service](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-unit_files). Serapeum can be accessed via <IP>:5000. I recommend putting a [reverse proxy (e.g. Apache)](https://www.digitalocean.com/community/tutorials/how-to-use-apache-http-server-as-reverse-proxy-using-mod_proxy-extension) in front of it so it is easier to access.
9. To create an admin user (there is only one type of user at the moment), execute _/var/opt/app/serapeum/scripts/serapeum_add_user_ and follow the instructions.