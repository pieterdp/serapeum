from flask import Blueprint, request
from flask_login import login_required
from serapeum.lib.wrapper.wrapper import RestWrapperApi
from serapeum.public.search.per_location.books import LibraryLocationBookApi
from serapeum.public.search.text import TextSearchApi
from serapeum.public.search.title import TitleSearchApi
from serapeum.public.search.author import AuthorSearchApi
from serapeum.public.search.series import SeriesSearchApi

search = Blueprint('search', __name__, url_prefix='/search/api')


def get_page():
    page = 1
    if request.args.get('page'):
        page = request.args.get('page')
    return page


@search.route('/query/<string:query>', methods=['GET'])
@login_required
def search_query(query):
    page = get_page()
    return RestWrapperApi(api_class=TextSearchApi, o_request=request, api_obj_id=query,
                          page=page).response


@search.route('/title/<string:title>', methods=['GET'])
@login_required
def search_title(title):
    page = get_page()
    return RestWrapperApi(api_class=TitleSearchApi, o_request=request, api_obj_id=title,
                          page=page).response


@search.route('/author/<string:author>', methods=['GET'])
@login_required
def search_author(author):
    page = get_page()
    return RestWrapperApi(api_class=AuthorSearchApi, o_request=request, api_obj_id=author,
                          page=page).response


@search.route('/series/<string:series_s>', methods=['GET'])
@login_required
def search_series(series_s):
    page = get_page()
    return RestWrapperApi(api_class=SeriesSearchApi, o_request=request, api_obj_id=series_s,
                          page=page).response


@search.route('/location/<string:location>')
@search.route('/location/book/<string:location>', methods=['GET'])
@login_required
def book_location(location):
    page = get_page()
    return RestWrapperApi(api_class=LibraryLocationBookApi, o_request=request, location=location,
                          page=page).response


@search.route('/location/rooms/<string:code>/cases/<string:case>/planks/<string:plank>/books', methods=['GET'])
@login_required
def book_plank(code, case, plank):
    page = get_page()
    return RestWrapperApi(api_class=LibraryLocationBookApi, o_request=request, code=code, case=case, plank=plank,
                          page=page).response
