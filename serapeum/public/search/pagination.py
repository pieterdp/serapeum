from sqlalchemy.ext.declarative import DeclarativeMeta
from serapeum.lib.exceptions import InvalidParameterException


class PaginationObject:

    def __init__(self, query, start=None, end=None, _all=False):
        if not _all and not start and not end:
            raise InvalidParameterException('Either _all or start and end are required.')
        if _all:
            self.results = list(query.all())
        else:
            self.results = list(query.slice(start, end).all())

    def __next__(self):
        if len(self.results) > 0:
            r = self.results.pop()
            return r
        else:
            raise StopIteration

    def __iter__(self):
        return self


class Pagination:

    def __init__(self, query, per_page=20, _total=None):
        if _total:
            self.total = _total
        else:
            self.total = query.count()
        self.max_pages = int(self.total / per_page)
        if self.total % per_page != 0:
            self.max_pages = self.max_pages + 1
        self.per_page = per_page
        self._query = query

    def page(self, page):
        page = int(page)
        if page > self.max_pages:
            raise StopIteration
        start = page * self.per_page - self.per_page  # page 1: 0 - 19; page 2: 20 - 21 etc.
        end = start + self.per_page - 1  # (see above)
        return PaginationObject(self._query, start, end)

    def all(self):
        return PaginationObject(self._query, _all=True)

