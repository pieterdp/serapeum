from serapeum.public.search import SearchApi
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import BookFormatter
from serapeum.lib.db.models.book import Book
from serapeum.lib.db.connection import connection


class TitleSearchApi(SearchApi):

    def __init__(self, *args, db=None, **kwargs):
        super(TitleSearchApi, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db

    def create(self, **kwargs):
        pass

    def read(self, item_id, page=1, **kwargs):
        query = self.db.session.query(Book).filter(Book.title.ilike('%{0}%'.format(item_id)))
        return self.format_output(Pagination(query), BookFormatter, page)

    def update(self, **kwargs):
        pass

    def delete(self, **kwargs):
        pass

    def list(self, **kwargs):
        pass
