

class AuthorFormatter:
    def __init__(self, author, follow=True):
        self._author = author
        self._follow = follow

    def format(self):
        f = {
            'id': self._author.id,
            'name': self._author.name
        }
        if self._follow:
            f['books'] = [BookFormatter(b).format() for b in self._author.books]
        return f


class LocationFormatter:
    def __init__(self, location, follow=True):
        self._location = location
        self._follow = follow

    def format(self):
        f = {
            'id': self._location.id,
            'room': self._location.room,
            'rack': self._location.rack,
            'plank': self._location.plank,
            'formatted': '.'.join([self._location.room, self._location.rack, self._location.plank])
        }
        if self._follow:
            f['books'] = [BookFormatter(b).format() for b in self._location.books]
        return f


class SubjectFormatter:
    def __init__(self, subject, follow=True):
        self._subject = subject
        self._follow = follow

    def format(self):
        f = {
            'id': self._subject.id,
            'name': self._subject.subject
        }
        if self._follow:
            f['books'] = [BookFormatter(b).format() for b in self._subject.books]
        return f


class SeriesFormatter:
    def __init__(self, series, follow=True):
        self._series = series
        self._follow = follow

    def format(self):
        f = {
            'id': self._series.id,
            'title': self._series.name,
            'material': self._series.material.name
        }
        if self._follow:
            f['books'] = [BookFormatter(b).format() for b in self._series.books]
        return f


class BookFormatter:

    def __init__(self, book):
        self._book = book

    def format(self):
        f = {
            'id': self._book.id,
            'title': self._book.title,
            'creators': [AuthorFormatter(a, follow=False).format() for a in self._book.creators],
            'location': '.'.join([self._book.location.room, self._book.location.rack, self._book.location.plank]),
            'subjects': [SubjectFormatter(s, follow=False).format() for s in self._book.subjects],
            'material': self._book.material.name
        }
        if self._book.series:
            f['series'] = SeriesFormatter(self._book.series, follow=False).format()
        else:
            f['series'] = None
        return f
