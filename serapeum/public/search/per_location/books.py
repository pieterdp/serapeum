from serapeum.public.search import SearchApi
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import BookFormatter
from serapeum.lib.db.models.book import Book
from serapeum.lib.db.models.location import Location
from serapeum.lib.exceptions import InvalidParameterException
from serapeum.lib.db.connection import connection
from flask_babel import gettext as _


class LibraryLocationBookApi(SearchApi):

    def __init__(self, *args, db=None, **kwargs):
        super(LibraryLocationBookApi, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db

    def create(self, **kwargs):
        pass

    def read(self, **kwargs):
        pass

    def update(self, **kwargs):
        pass

    def delete(self, **kwargs):
        pass

    def list(self, location=None, code=None, case=None, plank=None, page=1, **kwargs):
        if location:
            code, case, plank = location.split('.')
        if not code or not case or not plank:
            raise InvalidParameterException(_('Geef ofwel een location, ofwel een ruimte, kast en plank mee.'))
        query = self.db.session.query(Book) \
            .join(Location, Book.location) \
            .filter(
            Location.room == code,
            Location.rack == case,
            Location.plank == plank
        )
        return self.format_output(Pagination(query), BookFormatter, page)
