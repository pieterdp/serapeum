from flask import Blueprint, render_template, request, send_from_directory
from flask_login import login_required
from os.path import basename


public_gui = Blueprint('public_gui', __name__)


@public_gui.route('/', methods=['GET'])
def index():
    return render_template('public/main.html')


@public_gui.route('/books', methods=['GET'])
@public_gui.route('/books/', methods=['GET'])
@login_required
def book_search():
    query = ''
    if request.args.get('q'):
        query = request.args.get('q')
        if query == 'all':
            query = ''
    return render_template('public/search.html', query=query)


@public_gui.route('/search/result/<string:book_id>', methods=['GET'])
@login_required
def result(book_id):
    return render_template('public/books.html', book_id=book_id)
