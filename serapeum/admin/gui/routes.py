from flask import Blueprint, render_template, request, send_from_directory
from flask_login import login_required
from serapeum.lib.authentication.decorators import admin_required
from flask_babel import gettext as _


admin_gui = Blueprint('admin_gui', __name__)


@admin_gui.route('/admin', methods=['GET'])
@admin_required
@login_required
def admin():
    return render_template('admin/index.html')


@admin_gui.route('/admin/series', methods=['GET'])
@admin_required
@login_required
def admin_series():
    return render_template('admin/series/series/index.html')


@admin_gui.route('/admin/series/add', methods=['GET'])
@admin_required
@login_required
def admin_series_add():
    return render_template('admin/series/series/form.html')


@admin_gui.route('/admin/series/edit/<string:series_id>', methods=['GET'])
@admin_required
@login_required
def admin_series_edit(series_id):
    return render_template('admin/series/series/form.html', edit=True)


@admin_gui.route('/admin/series/list', methods=['GET'])
@admin_required
@login_required
def admin_series_list():
    return render_template('admin/series/series/list.html')


@admin_gui.route('/admin/journals', methods=['GET'])
@admin_required
@login_required
def admin_journals():
    return render_template('admin/series/journals/index.html')


@admin_gui.route('/admin/series/journals/add', methods=['GET'])
@admin_required
@login_required
def admin_journals_add():
    return render_template('admin/series/series/form.html', section_title=_('Tijdschrift toevoegen'))


@admin_gui.route('/admin/series/journals/edit/<string:series_id>', methods=['GET'])
@admin_required
@login_required
def admin_journals_edit(series_id):
    return render_template('admin/series/series/form.html', section_title=_('Tijdschrift bewerken'))


@admin_gui.route('/admin/series/journals/list', methods=['GET'])
@admin_required
@login_required
def admin_journals_list():
    return render_template('admin/series/series/list.html', section_title=_('Tijdschriften'))


@admin_gui.route('/admin/comics', methods=['GET'])
@admin_required
@login_required
def admin_comics():
    return render_template('admin/series/comics/index.html')


@admin_gui.route('/admin/series/comics/add', methods=['GET'])
@admin_required
@login_required
def admin_comics_add():
    return render_template('admin/series/series/form.html', section_title=_('Stripreeks toevoegen'))


@admin_gui.route('/admin/series/comics/edit/<string:series_id>', methods=['GET'])
@admin_required
@login_required
def admin_comics_edit(series_id):
    return render_template('admin/series/series/form.html', section_title=_('Stripreeks bewerken'))


@admin_gui.route('/admin/series/comics/list', methods=['GET'])
@admin_required
@login_required
def admin_comics_list():
    return render_template('admin/series/series/list.html', section_title=_('Stripreeksen'))


@admin_gui.route('/admin/book/manual', methods=['GET'])
@admin_gui.route('/admin/books/add', methods=['GET'])
@admin_required
@login_required
def admin_book_manual():
    return render_template('admin/books/form.html')


@admin_gui.route('/admin/books/edit/<string:book_id>', methods=['GET'])
@admin_required
@login_required
def admin_book_edit(book_id):
    return render_template('admin/books/form.html', edit=True)


@admin_gui.route('/admin/comics/add', methods=['GET'])
@admin_required
@login_required
def admin_comic_add():
    return render_template('admin/books/special_books/comics/form.html')


@admin_gui.route('/admin/locations', methods=['GET'])
@admin_required
@login_required
def admin_locations():
    return render_template('admin/locations/index.html')
