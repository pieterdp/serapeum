from serapeum.lib.api import Api
from serapeum.lib.exceptions import ItemNotFoundException
from serapeum.lib.db.models.book import Book
from serapeum.lib.db.models.creator import Creator
from serapeum.lib.db.models.location import Location
from serapeum.lib.db.models.material import Material
from serapeum.lib.db.models.series import Series
from serapeum.lib.db.models.subject import Subject
from serapeum.lib.db.connection import connection
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import BookFormatter
from flask_babel import gettext as _


class LibraryBooks(Api):
    required = ['creator', 'title', 'location']
    valid = ['series', 'subjects', 'material']

    def __init__(self, *args, db=None, format=False, **kwargs):
        super(LibraryBooks, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db
        self._format = format

    def create(self, input_data, format=False, **kwargs):
        if isinstance(input_data, list):
            results = []
            for item in input_data:
                results.append(self.create(item, format=format, **kwargs))
            return results
        self.check_input_data(input_data)

        creators = input_data['creator']
        if not isinstance(creators, list):
            creators = [creators]

        subjects = []
        if 'subjects' in input_data:
            if not isinstance(input_data['subjects'], list):
                subjects = [input_data['subjects']]
            else:
                subjects = input_data['subjects']

        series = None
        if 'series' in input_data:
            series = input_data['series']

        material = _('Boek')
        if 'material' in input_data:
            material = input_data['material']

        book = Book(
            title=input_data['title'],
            creators=creators,
            location=input_data['location'],
            subjects=subjects,
            series=series,
            material=material,
            db=self.db
        )

        # You can have twice (or more) the same book
        self.db.session.add(book)
        self.db.session.commit()

        return self.read(book.id, format=format)

    def read(self, item_id, format=False, **kwargs):
        book = self.db.session.query(Book).filter(Book.id == item_id).first()
        self.db.session.commit()
        if not book:
            raise ItemNotFoundException('No book with id {0}.'.format(item_id))
        if self._format or format:
            return BookFormatter(book).format()
        else:
            return book

    def update(self, item_id, input_data, format=False, **kwargs):
        book = self.read(item_id)

        # TODO move someplace else/improve

        if 'creator' in input_data:
            creators = input_data['creator']
            if not isinstance(creators, list):
                creators = [creators]
            _c = []
            for creator_name in creators:
                c = self.db.session.query(Creator).filter(Creator.name == creator_name).first()
                if not c:
                    c = Creator(creator_name)
                    self.db.session.add(c)
                _c.append(c)
            book.creators = _c
        if 'title' in input_data:
            book.title = input_data['title']
        if 'location' in input_data:
            location = input_data['location']
            if not isinstance(location, dict):
                location_a = location.split('.')
                location = {
                    'room': location_a[0],
                    'rack': location_a[1],
                    'plank': location_a[2]
                }
            loc = self.db.session.query(Location).filter(
                Location.room == location['room'],
                Location.rack == location['rack'],
                Location.plank == location['plank']
            ).first()
            if not loc:
                loc = Location(**location)
                self.db.session.add(loc)
            book.location = loc
        if 'series' in input_data:
            series = input_data['series']
            se = self.db.session.query(Series).filter(Series.name == series).first()
            if not se:
                if 'material' not in input_data or not input_data['material']:
                    material = _('Boek')
                else:
                    material = input_data['material']
                se = Series(series, material, db=self.db)
                self.db.session.add(se)
            book.series = se
        if 'subjects' in input_data:
            subjects = input_data['subjects']
            if not isinstance(subjects, list):
                subjects = [subjects]
            _su = []
            for subject in subjects:
                s = self.db.session.query(Subject).filter(Subject.subject == subject).first()
                if not s:
                    s = Subject(subject)
                    self.db.session.add(s)
                _su.append(s)
            book.subjects = _su
        if 'material' in input_data:
            material = input_data['material']
            m = self.db.session.query(Material).filter(Material.name == material).first()
            if not m:
                m = Material(material)
                self.db.session.add(m)
            book.material = m

        book.update_search()
        self.db.session.commit()
        if self._format or format:
            return BookFormatter(book).format()
        else:
            return book

    def delete(self, item_id, **kwargs):
        self.db.session.query(Book).filter(Book.id == item_id).delete()
        self.db.session.commit()

    def list(self, page=None, **kwargs):
        query = self.db.session.query(Book)
        if page:
            if self._format:
                return self.format_output(Pagination(query), BookFormatter, page)
            else:
                return Pagination(query).page(page)
        return query.all()

    def by_title(self, title):
        book = self.db.session.query(Book).filter(Book.title == title).first()
        if not book:
            raise ItemNotFoundException('No book with title {0}.'.format(title))
        if self._format:
            return BookFormatter(book).format()
        else:
            return book
