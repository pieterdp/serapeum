from serapeum.lib.api import Api
from serapeum.lib.exceptions import ItemNotFoundException
from serapeum.lib.db.models.subject import Subject
from serapeum.lib.db.connection import connection
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import SubjectFormatter


class LibrarySubjects(Api):

    def __init__(self, *args, db=None, **kwargs):
        super(LibrarySubjects, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db

    def create(self, input_data, **kwargs):
        pass

    def read(self, item_id, format=False, **kwargs):
        subject = self.db.session.query(Subject).filter(Subject.subject == item_id).first()
        if not subject:
            raise ItemNotFoundException('No subject called {0}.'.format(item_id))
        if format:
            return SubjectFormatter(subject).format()
        return subject

    def update(self, item_id, input_data, **kwargs):
        pass

    def delete(self, item_id, **kwargs):
        pass

    def list(self, page=1, _all=False, **kwargs):
        query = self.db.session.query(Subject)
        if _all:
            return self.format_output(Pagination(query), SubjectFormatter, None, {'follow': False})
        return self.format_output(Pagination(query), SubjectFormatter, page, {'follow': False})
