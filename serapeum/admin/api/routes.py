from flask import Blueprint, request
from flask_login import login_required
from serapeum.lib.wrapper.wrapper import RestWrapperApi
from serapeum.admin.api.books import LibraryBooks
from serapeum.admin.api.series import LibrarySeries
from serapeum.admin.api.journals import LibraryJournals
from serapeum.admin.api.authors import LibraryAuthors
from serapeum.admin.api.locations import LibraryLocations
from serapeum.admin.api.locations.per_type import LocationPerTypeApi
from serapeum.admin.api.subjects import LibrarySubjects
from serapeum.admin.api.comics import LibraryComics


library = Blueprint('library', __name__, url_prefix='/library/api')


def get_page():
    page = 1
    if request.args.get('page'):
        page = request.args.get('page')
    return page


def get_all():
    _all = False
    if request.args.get('all'):
        if request.args.get('all').lower() == 'true'.lower():
            _all = True
    return _all


def get_by_name():
    _by_name = False
    if request.args.get('name'):
        if request.args.get('name').lower() == 'true'.lower():
            _by_name = True
    return _by_name


def get_follow():
    _follow = True
    if request.args.get('follow'):
        if request.args.get('follow').lower() == 'false'.lower():
            _follow = False
    return _follow


@library.route('/books', methods=['GET', 'POST'])
@library.route('/books/<string:book_id>', methods=['GET', 'PUT', 'DELETE'])
@login_required
def books(book_id=None):
    page = get_page()
    return RestWrapperApi(api_class=LibraryBooks, o_request=request,
                          api_obj_id=book_id, page=page, format=True).response


@library.route('/series', methods=['GET', 'POST'])
@library.route('/series/<string:series_id>', methods=['GET', 'DELETE', 'PUT'])
@login_required
def series(series_id=None):
    page = get_page()
    _all = get_all()
    by_name = get_by_name()
    if _all:
        page = None
    _follow = get_follow()
    return RestWrapperApi(api_class=LibrarySeries, o_request=request,
                          api_obj_id=series_id, page=page, format=True, _follow=_follow,
                          _all=_all, by_name=by_name).response


@library.route('/journals', methods=['GET', 'POST'])
@library.route('/journals/<string:journal_name>', methods=['GET', 'DELETE'])
@login_required
def journals(journal_name=None):
    page = get_page()
    _all = get_all()
    by_name = get_by_name()
    if _all:
        page = None
    _follow = get_follow()
    return RestWrapperApi(api_class=LibraryJournals, o_request=request,
                          api_obj_id=journal_name, page=page, format=True, _follow=_follow,
                          _all=_all, by_name=by_name).response


@library.route('/comics', methods=['GET', 'POST'])
@library.route('/comics/<string:comic_name>', methods=['GET', 'DELETE'])
@login_required
def comics(comic_name=None):
    page = get_page()
    _all = get_all()
    by_name = get_by_name()
    if _all:
        page = None
    _follow = get_follow()
    return RestWrapperApi(api_class=LibraryComics, o_request=request,
                          api_obj_id=comic_name, page=page, format=True, _follow=_follow,
                          _all=_all, by_name=by_name).response


@library.route('/authors', methods=['GET'])
@library.route('/authors/<string:author>', methods=['GET'])
@login_required
def authors(author=None):
    _all = get_all()
    page = None
    if not _all:
        page = get_page()
    return RestWrapperApi(api_class=LibraryAuthors, o_request=request,
                          api_obj_id=author, page=page, format=True, _all=_all).response


@library.route('/subjects', methods=['GET'])
@library.route('/subjects/<string:subject>', methods=['GET'])
@login_required
def subjects(subject=None):
    _all = get_all()
    page = None
    if not _all:
        page = get_page()
    return RestWrapperApi(api_class=LibrarySubjects, o_request=request,
                          api_obj_id=subject, page=page, format=True, _all=_all).response


@library.route('/locations', methods=['GET', 'POST'])
@library.route('/locations/<string:location_code>', methods=['GET', 'PUT'])
@login_required
def locations(location_code=None):
    _all = get_all()
    page = None
    if not _all:
        page = get_page()
    return RestWrapperApi(api_class=LibraryLocations, o_request=request,
                          api_obj_id=location_code, page=page, format=True, _all=_all).response


@library.route('/locations/rooms', methods=['GET'])
@login_required
def rooms():
    _all = get_all()
    page = None
    if not _all:
        page = get_page()
    return RestWrapperApi(api_class=LocationPerTypeApi, o_request=request, format=True,
                          page=page, _all=_all).response


@library.route('/locations/rooms/<string:room_name>/cases', methods=['GET'])
@login_required
def cases(room_name):
    _all = get_all()
    page = None
    if not _all:
        page = get_page()
    return RestWrapperApi(api_class=LocationPerTypeApi, o_request=request, format=True,
                          room_name=room_name, page=page, _all=_all).response


@library.route('/locations/rooms/<string:room_name>/cases/<string:case_name>/planks', methods=['GET'])
@login_required
def planks(room_name, case_name):
    _all = get_all()
    page = None
    if not _all:
        page = get_page()
    return RestWrapperApi(api_class=LocationPerTypeApi, o_request=request, format=True,
                          room_name=room_name, case_name=case_name, page=page, _all=_all).response
