from serapeum.lib.api import Api
from serapeum.lib.db.models.creator import Creator
from serapeum.lib.db.connection import connection
from serapeum.lib.exceptions import ItemNotFoundException
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import AuthorFormatter


class LibraryAuthors(Api):
    required = ['name']

    def __init__(self, *args, db=None, **kwargs):
        super(LibraryAuthors, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db

    def create(self, input_data, format=False, **kwargs):
        try:
            return self.read(input_data['name'], format=format)
        except ItemNotFoundException:
            c = Creator(input_data['name'])
            self.db.session.add(c)
            self.db.session.commit()
            if format:
                return AuthorFormatter(c).format()
            return c

    def read(self, item_id, format=False, **kwargs):
        author = item_id
        c = self.db.session.query(Creator).find(Creator.name == author).first()
        if not c:
            raise ItemNotFoundException('No creator called {0}.'.format(author))
        if format:
            return AuthorFormatter(c).format()
        return c

    def update(self, item_id, input_data, format=False, **kwargs):
        pass

    def delete(self, item_id, **kwargs):
        pass

    def list(self, page=1, _all=False, **kwargs):
        query = self.db.session.query(Creator)
        if _all:
            return self.format_output(Pagination(query), AuthorFormatter, None, {'follow': False})
        return self.format_output(Pagination(query), AuthorFormatter, page, {'follow': False})

    def by_name(self, name):
        return self.read(name)
