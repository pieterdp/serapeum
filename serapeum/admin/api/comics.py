from serapeum.lib.db.models.material import Material
from serapeum.lib.db.models.series import Series
from serapeum.admin.api.books import LibraryBooks
from serapeum.admin.api.series import LibrarySeries
from serapeum.lib.db.connection import connection
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import SeriesFormatter
from flask_babel import gettext as _


class LibraryComics(LibrarySeries):
    required = ['name']
    valid = ['comics', 'creator']

    def __init__(self, *args, db=None, **kwargs):
        super(LibraryComics, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db
        self.books_a = LibraryBooks(db=self.db)
        self.series_a = LibrarySeries(db=self.db)

    def create(self, input_data, format=False, **kwargs):
        self.check_input_data(input_data)
        if 'comics' in input_data:
            books = []
            for book in input_data['comics']:
                book['material'] = _('Stripverhaal')
                books.append(book)
        else:
            books = []

        params = {
            'name': input_data['name'],
            'material': 'Stripverhaal',
            'books': books
        }
        if 'creator' in input_data:
            params['creator'] = input_data['creator']

        return self.series_a.create(params, format=format)

    def read(self, item_id, format=False, **kwargs):
        return self.series_a.read(item_id, format=format)

    def update(self, item_id, input_data, format=False, **kwargs):
        self.check_input_data(input_data)
        series_name = item_id

        if 'comics' in input_data:
            books = []
            for book in input_data['comics']:
                book['material'] = _('Stripverhaal')
                books.append(book)
        else:
            books = []

        params = {
            'name': input_data['name'],
            'material': _('Stripverhaal'),
            'books': books
        }
        if 'creator' in input_data:
            params['creator'] = input_data['creator']

        return self.series_a.update(
            series_name,
            params,
            format=format
        )

    def delete(self, item_id, **kwargs):
        self.series_a.delete(item_id)

    def list(self, page=None, raw=False, _follow=True, **kwargs):
        material = self.db.session.query(Material).filter(Material.name == _('Comic')).first()
        if not material:
            return []
        query = self.db.session.query(Series).filter(Series.material == material)
        if page:
            return self.series_count(self.format_output(Pagination(query), SeriesFormatter, page, {'follow': _follow}))
        if raw:
            return query.all()
        return self.series_count(self.format_output(Pagination(query), SeriesFormatter, None, {'follow': _follow}))

    def add_comic(self, series, book):
        if not isinstance(book, list):
            book = [book]
        for comic in book:
            if 'id' in comic:
                comic_o = self.books_a.read(comic['id'])
                comic_o.series = series
            else:
                comic['series'] = series.name
                self.books_a.create(comic)

        self.db.session.commit()

        return series
