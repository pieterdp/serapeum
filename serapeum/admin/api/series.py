from serapeum.lib.api import Api
from serapeum.lib.exceptions import ItemNotFoundException
from serapeum.lib.db.models.material import Material
from serapeum.lib.db.models.series import Series
from serapeum.lib.db.models.book import Book
from serapeum.admin.api.books import LibraryBooks
from serapeum.lib.db.connection import connection
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import SeriesFormatter
from flask_babel import gettext as _


class LibrarySeries(Api):
    required = ['name', 'books']
    valid = ['creator', 'material']

    def __init__(self, *args, db=None, **kwargs):
        super(LibrarySeries, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db
        self.books_a = LibraryBooks(db=self.db)

    def create(self, input_data, format=False, **kwargs):
        if isinstance(input_data, list):
            results = []
            for item in input_data:
                results.append(self.create(item, **kwargs))
            return results
        self.check_input_data(input_data)
        try:
            series = self.read(input_data['name'])
        except ItemNotFoundException:
            material = _('Boek')
            if 'material' in input_data:
                material = input_data['material']

            series = Series(
                name=input_data['name'],
                material=material,
                db=self.db
            )
            self.db.session.add(series)

        if not isinstance(input_data['books'], list):
            books = [input_data['books']]
        else:
            books = input_data['books']

        for book in books:
            series = self.add_book_to_series(series, book)

        self.db.session.commit()
        return self.read(series.name, format=format)

    def read(self, item_id, format=False, by_name=True, **kwargs):
        if not by_name:
            return self.by_id(item_id, format)
        series_name = item_id
        series = self.db.session.query(Series).filter(Series.name == series_name).first()
        self.db.session.commit()
        if not series:
            raise ItemNotFoundException('No series called {0}.'.format(series_name))
        if format:
            return SeriesFormatter(series).format()
        return series

    def update(self, item_id, input_data, by_name=True, format=False, **kwargs):
        self.check_input_data(input_data)
        series = self.read(item_id, by_name=by_name)
        series.name = input_data['name']

        material = _('Boek')
        if 'material' in input_data:
            material = input_data['material']
        material_o = self.db.session.query(Material).filter(Material.name == material).first()
        if not material_o:
            material_o = Material(material)
        series.material = material_o

        if not isinstance(input_data['books'], list):
            books = [input_data['books']]
        else:
            books = input_data['books']

        for book in books:
            series = self.add_book_to_series(series, book)

        self.db.session.commit()

        if format:
            return SeriesFormatter(series).format()
        return series

    def delete(self, item_id, **kwargs):
        self.db.session.query(Series).filter(Series.id == item_id).delete()
        self.db.session.commit()

    def list(self, page=None, raw=False, _follow=True, **kwargs):
        query = self.db.session.query(Series).order_by(Series.name.desc())
        if page:
            return self.series_count(self.format_output(Pagination(query), SeriesFormatter, page, {'follow': _follow}))
        if raw:
            return query.all()
        return self.series_count(self.format_output(Pagination(query), SeriesFormatter, None, {'follow': _follow}))

    def add_book_to_series(self, series, book, _commit=False):
        if 'id' in book:
            book_o = self.books_a.read(book['id'])
            book_o.series = series
        else:
            try:
                book_db = self.books_a.by_title(book['title'])
            except ItemNotFoundException:
                book['series'] = series.name
                self.books_a.create(book)
            else:
                book_db.series = series

        if _commit:
            self.db.session.commit()

        return series

    def by_name(self, series_name, format=False):
        return self.read(series_name, format=format)

    def by_id(self, series_id, format=False):
        series = self.db.session.query(Series).filter(Series.id == series_id).first()
        if not series:
            raise ItemNotFoundException('No series with id {0}.'.format(series_id))
        if format:
            return SeriesFormatter(series).format()
        return series

    def series_count(self, orig):
        out = orig.copy()
        out['results'] = []
        for series in orig['results']:
            result = series
            result['amount'] = self.db.session.query(Book).filter(Book.series_id == series['id']).count()
            out['results'].append(result)
        return out
