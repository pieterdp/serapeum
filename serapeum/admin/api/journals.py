from serapeum.lib.db.models.material import Material
from serapeum.lib.db.models.series import Series
from flask_babel import gettext as _
from serapeum.admin.api.series import LibrarySeries
from serapeum.admin.api.books import LibraryBooks
from serapeum.lib.db.connection import connection
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import SeriesFormatter
# Series and books of type = journal


class LibraryJournals(LibrarySeries):
    required = ['name', 'editions']

    def __init__(self, *args, db=None, **kwargs):
        super(LibraryJournals, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db
        self.books_a = LibraryBooks(db=self.db)
        self.series_a = LibrarySeries(db=self.db)

    def create(self, input_data, format=False, **kwargs):
        self.check_input_data(input_data)

        books = []
        for edition in input_data['editions']:
            title = input_data['name']
            if 'number' in edition and edition['number'] is not None and edition['number'] != '':
                title = title + _(', Nummer %(number)s', number=edition['number'])
            if 'volume' in edition and edition['volume'] is not None and edition['volume'] != '':
                title = title + _(', Deel %(volume)s', volume=edition['volume'])
            if 'edition' in edition and edition['edition'] is not None and edition['edition'] != '':
                title = title + _(', Jaargang %(edition)s', edition=edition['edition'])
            edition['title'] = title
            edition['material'] = _('Tijdschrift')
            if 'creator' not in edition or edition['creator'] is None or len(edition['creator']) == 0:
                edition['creator'] = input_data['name']
            books.append(edition)

        return self.series_a.create({
            'name': input_data['name'],
            'material': _('Tijdschrift'),
            'books': books
        }, format=format)

    def read(self, item_id, format=False, **kwargs):
        return self.series_a.by_name(item_id, format=format)

    def update(self, item_id, input_data, format=False, **kwargs):
        self.check_input_data(input_data)

        books = []
        for edition in input_data['editions']:
            title = input_data['name']
            if 'number' in edition and edition['number'] is not None and edition['number'] != '':
                title = title + _(', Nummer %(number)s', number=edition['number'])
            if 'volume' in edition and edition['volume'] is not None and edition['volume'] != '':
                title = title + _(', Deel %(volume)s', volume=edition['volume'])
            if 'edition' in edition and edition['edition'] is not None and edition['edition'] != '':
                title = title + _(', Jaargang %(edition)s', edition=edition['edition'])
            edition['title'] = title
            edition['material'] = _('Tijdschrift')
            if 'creator' not in edition or edition['creator'] is None or len(edition['creator']) == 0:
                edition['creator'] = input_data['name']
            books.append(edition)

        return self.series_a.update(item_id, {
            'name': input_data['name'],
            'material': _('Journal'),
            'books': books
        }, format=format)

    def delete(self, item_id, **kwargs):
        self.series_a.delete(item_id)

    def list(self, page=None, **kwargs):
        material = self.db.session.query(Material).filter(Material.name == _('Journal')).first()
        if not material:
            return []
        query = self.db.session.query(Series).filter(Series.material == material)
        if page:
            return self.format_output(Pagination(query), SeriesFormatter, page)
        return query.all()
