from serapeum.lib.exceptions import InvalidParameterException
import re
from serapeum.public.search import SearchApi
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import LocationFormatter
from serapeum.lib.db.models.location import Location
from serapeum.lib.db.connection import connection
from flask_babel import gettext as _


class LibraryLocations(SearchApi):
    required = ['code', 'b_case', 'plank']

    def __init__(self, *args, db=None, **kwargs):
        super(LibraryLocations, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db

    def create(self, input_data, **kwargs):
        if isinstance(input_data, list):
            results = []
            for item in input_data:
                results.append(self.create(item, **kwargs))
            return results
        self.check_input_data(input_data)
        location = self.db.session.query(Location).filter(
            Location.room == input_data['code'],
            Location.rack == input_data['b_case'],
            Location.plank == input_data['plank']
        ).first()
        if not location:
            location = Location(input_data['code'], input_data['b_case'], input_data['plank'])
            self.db.session.add(location)
            self.db.session.commit()
        return LocationFormatter(location, follow=False)

    def read(self, item_id, **kwargs):
        pass

    def update(self, item_id, input_data, **kwargs):
        pass

    def delete(self, item_id, **kwargs):
        self.db.session.query(Location).filter(Location.id == item_id).delete()
        self.db.session.commit()

    def list(self, page=1, _all=False, raw=False, **kwargs):
        query = self.db.session.query(Location)
        if raw:
            return query.all()
        if _all:
            return self.format_output(Pagination(query), LocationFormatter, None)
        return self.format_output(Pagination(query), LocationFormatter, page)

    @staticmethod
    def parse_location(location):
        location_re = re.compile('^([A-Z]+)([0-9]+)\.([0-9]+)$', flags=re.IGNORECASE)
        m = location_re.match(location)
        if not m:
            raise InvalidParameterException(_('Niet mogelijk om locatie %(location)s op te delen.', location=location))
        return {
            'code': m.group(1),
            'case': m.group(2),
            'plank': m.group(3)
        }
