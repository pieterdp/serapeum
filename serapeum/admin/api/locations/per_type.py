from serapeum.admin.api.locations import LibraryLocations
from serapeum.lib.db.models.location import Location
from serapeum.public.search.pagination import Pagination
from serapeum.public.search.formatter import LocationFormatter
from serapeum.lib.exceptions import InvalidParameterException
from flask_babel import gettext as _


class LocationPerTypeApi(LibraryLocations):

    def list(self, room_name=None, case_name=None, page=1, _all=False, **kwargs):
        if not room_name and not case_name:
            query = self.db.session.query(Location).distinct(Location.room)
        elif room_name and not case_name:
            query = self.db.session.query(Location).filter(Location.room == str(room_name)).distinct(Location.rack)
        elif case_name and not room_name:
            raise InvalidParameterException(_('Ik heb een kast en een ruimte nodig om een lijst met planken te geven.'))
        else:
            query = self.db.session.query(Location).filter(
                Location.room == str(room_name),
                Location.rack == str(case_name)
            ).distinct(Location.plank)
        if _all:
            return self.format_output(Pagination(query), LocationFormatter, None, {'follow': False})
        return self.format_output(Pagination(query), LocationFormatter, page, {'follow': False})
