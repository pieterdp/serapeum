import json
from os.path import isfile
from serapeum.lib.exceptions import InvalidParameterException
from flask_babel import gettext as _


class Config:

    def __init__(self, config_file='config/settings.json'):
        if not isfile(config_file):
            raise InvalidParameterException(_('Het configuratiebestand "%(config_file)s" bestaat niet.',
                                              config_file=config_file))
        with open(config_file, 'r') as fh:
            self._config = json.load(fh)

    def get(self, key):
        try:
            return self._config[key]
        except KeyError:
            raise InvalidParameterException(_('De waarde %(key)s bestaat niet in het configuratiebestand.', key=key))

    def set(self, key, value):
        self._config[key] = value
