from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired
from flask_babel import gettext as _


class LoginForm(FlaskForm):
    username = StringField(_('Gebruikersnaam'), validators=[DataRequired()])
    password = PasswordField(_('Wachtwoord'), validators=[DataRequired()])
