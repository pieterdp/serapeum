from serapeum.lib.db.models.user import User
from serapeum.lib.db.connection import connection
from serapeum.lib.api import Api
from serapeum.lib.exceptions import ItemNotFoundException, ItemExistsException
from flask_babel import gettext as _


class UserApi(Api):
    required = ['username', 'password']
    valid = ['roles']

    def __init__(self, *args, db=None, **kwargs):
        super(UserApi, self).__init__(*args, **kwargs)
        if not db:
            self.db = connection
        else:
            self.db = db

    def create(self, input_data, **kwargs):
        self.check_input_data(input_data)
        roles = ['user']
        if 'roles' in input_data:
            if not isinstance(input_data['roles'], list):
                roles = [input_data['roles']]
            else:
                roles = input_data['roles']

        user_m = self.db.session.query(User).filter(User.username == input_data['username']).first()
        if not user_m:
            user_m = User(input_data['username'], '!', roles)
            user_m.set_password(input_data['password'])
            self.db.session.add(user_m)
        else:
            raise ItemExistsException(_('De gebruiker %(username)s bestaat al.', username=user_m.username))

        self.db.session.commit()
        return self.by_id(user_m.id)

    def read(self, username, **kwargs):
        user = self.db.session.query(User).filter(User.username == username).first()
        if not user:
            raise ItemNotFoundException(_('De gebruiker %(username)s bestaat niet.', username=username))
        return user

    def update(self, username, user, **kwargs):
        pass

    def delete(self, username, **kwargs):
        pass

    def list(self, **kwargs):
        pass

    def by_id(self, user_id):
        return self.db.session.query(User).filter(User.id == user_id).first()

    def update_password(self, user_id, password):
        user = self.by_id(user_id)
        if not user:
            raise ItemNotFoundException(_('De gebruiker %(username)s bestaat niet.', username=user_id))
        user.set_password(password)
        self.db.session.commit()
