from flask import render_template, Blueprint, request, flash, redirect, url_for
from flask_login import login_user, login_required, logout_user, fresh_login_required
from flask_babel import gettext as _
from urllib.parse import urlparse, urljoin
from serapeum.lib.authentication.login import LoginForm
from serapeum.lib.authentication import UserApi
from logging import getLogger


logger = getLogger('flask.app')


def is_safe_url(target):
    url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    if test_url.scheme in ('http', 'https') and \
           url.netloc == test_url.netloc:
        return True
    return False


auth = Blueprint('auth', __name__, url_prefix='/auth')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    next_url = request.args.get('next')
    if not is_safe_url(next_url):
        next_url = None
    if form.validate_on_submit():
        try:
            user = UserApi().read(form.username.data)
        except Exception as e:
            flash(_('Gebruikersnaam of wachtwoord was onjuist.'))
            logger.warning(e)
            return render_template('lib/login/form.html', next=next_url, form=form)
        if user.password_ok(form.password.data):
            login_user(user)
            flash(_('U bent nu aangemeld.'))
            return redirect(next_url or url_for('public_gui.index'))
        else:
            flash(_('Gebruikersnaam of wachtwoord was onjuist.'))
    return render_template('lib/login/form.html', next=next_url, form=form)


@auth.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    next_url = request.args.get('next')
    if not is_safe_url(next_url):
        next_url = None
    logout_user()
    return redirect(next_url or url_for('public_gui.index'))
