from functools import wraps
from flask import request, redirect, url_for
from flask_login import current_user


def admin_required(f):
    @wraps(f)
    def wrapped_function(*args, **kwargs):
        if not current_user or current_user.is_anonymous or not current_user.is_admin:
            return redirect(url_for('auth.login', next=request.full_path))
        return f(*args, **kwargs)
    return wrapped_function
