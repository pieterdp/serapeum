
class InvalidGroupException(Exception):
    pass


class ItemNotFoundException(Exception):
    pass


class ItemExistsException(Exception):
    pass


class CannotCreateItemException(Exception):
    pass


class InvalidOrMissingUserTypeException(Exception):
    pass


class InvalidUserOrPasswordException(Exception):
    pass


class InvalidParameterException(Exception):
    pass


class MissingParameterException(Exception):
    pass

