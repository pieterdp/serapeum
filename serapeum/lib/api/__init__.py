from abc import ABCMeta, abstractmethod
from functools import wraps
from serapeum.lib.exceptions import MissingParameterException
from flask_babel import gettext as _
from logging import getLogger
import re


def check_required_arguments(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        inst = args[0]
        # args[0] = self
        inst.check_input_data(*args[1:], **kwargs)
        return f(*args, **kwargs)
    return wrapper


class Api:
    __metaclass__ = ABCMeta

    def __init__(self, *args, **kwargs):
        self.api = []
        self.logger = getLogger('flask.app')

    @check_required_arguments
    @abstractmethod
    def create(self, **kwargs):
        return

    @abstractmethod
    def read(self, **kwargs):
        return

    @abstractmethod
    def update(self, **kwargs):
        return

    @abstractmethod
    def delete(self, **kwargs):
        return

    @abstractmethod
    def list(self, **kwargs):
        return

    def check_input_data(self, input_data, required=None, strict=False):
        if hasattr(self, 'required') and not required:
            required = self.required
        for r in required:
            if r not in input_data or input_data[r] is None:
                raise MissingParameterException(_('U moet een waarde opgeven voor %(parameter)s.', parameter=r))
            if strict:
                if len(input_data[r]) == 0:
                    raise MissingParameterException(_('U moet een waarde opgeven voor %(parameter)s.', parameter=r))

    def default_parameter(self, input_data, defaults=None):
        if hasattr(self, 'defaults') and not defaults:
            defaults = self.defaults
        input_data_with_defaults = input_data.copy()
        for d in defaults.keys():
            if d not in input_data:
                input_data_with_defaults[d] = defaults[d]
        return input_data_with_defaults

    def caller(self, func, **kwargs):
        r = []
        for a in self.api:
            f = getattr(a, func)
            r.append(f(**kwargs))
        return r
    
    @staticmethod
    def format_output(pagination_o, formatter, page, formatter_params=None):
        if not formatter_params:
            formatter_params = {}
        output = []
        output_obj = {
            'page': page,
            'total': pagination_o.total,
            'pages': pagination_o.max_pages,
            'per_page': pagination_o.per_page,
            'results': []
        }
        try:
            if not page:
                iterator = pagination_o.all()
            else:
                iterator = pagination_o.page(page)
            for el in iterator:
                output.append(formatter(el, **formatter_params).format())
            output_obj['results'] = output
        except StopIteration:
            pass
        return output_obj
