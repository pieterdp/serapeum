from serapeum.lib.db.models import Model
from sqlalchemy import Column, String, Integer, Text, Index, func


class Subject(Model):
    __tablename__ = 'subjects'

    id = Column(Integer, primary_key=True)
    subject = Column(Text, index=True, unique=True)

    def __init__(self, subject):
        self.subject = subject
