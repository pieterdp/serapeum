from serapeum.lib.db.models import Model
from sqlalchemy import Column, String, Integer, Text, Index, func
from sqlalchemy.orm import relationship


class Material(Model):
    __tablename__ = 'materials'

    id = Column(Integer, primary_key=True)
    name = Column(Text, index=True, unique=True)
    books = relationship('Book')
    series = relationship('Series')

    def __init__(self, name):
        self.name = name
