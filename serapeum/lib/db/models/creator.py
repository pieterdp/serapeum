from serapeum.lib.db.models import Model
from sqlalchemy import Column, String, Integer, Text, Index, func


class Creator(Model):
    __tablename__ = 'creators'

    id = Column(Integer, primary_key=True)
    name = Column(Text, index=True)

    def __init__(self, name):
        self.name = name
