from serapeum.lib.db.models import Model
from passlib.context import CryptContext
from sqlalchemy import Column, String, Integer, Text, Table, ForeignKey
from sqlalchemy.orm import relationship, backref
from serapeum.lib.db.models.role import Role
from serapeum.lib.db.connection import connection


user_roles_assocation = Table(
    'user_roles',
    Model.metadata,
    Column('user_id', Integer, ForeignKey('users.id', ondelete='CASCADE', onupdate='CASCADE'), index=True),
    Column('role_id', Integer, ForeignKey('roles.id', ondelete='CASCADE', onupdate='CASCADE'), index=True)
)


class User(Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(256), index=True)
    password = Column(String(512))
    roles = relationship('Role', secondary=user_roles_assocation)

    def __init__(self, username, password_hash, roles=(), db=None):
        if not db:
            db = connection
        self.username = username
        self.password = password_hash
        if len(roles) == 0:
            roles = ['user']
        roles_m = []
        for role in roles:
            if not isinstance(role, dict):
                role = {'name': role}
            role_m = db.session.query(Role).filter(Role.name == role['name']).first()
            if not role_m:
                role_m = Role(**role)
                db.session.add(role_m)
            roles_m.append(role_m)
        self.roles = roles_m
        db.session.close()

    def set_password(self, password):
        __context = CryptContext(schemes=['pbkdf2_sha256'])
        self.password = __context.hash(password)

    def password_ok(self, password):
        __context = CryptContext(schemes=['pbkdf2_sha256'])
        if __context.verify(password, self.password):
            return True
        return False

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def has_role(self, role):
        roles = [r.name for r in self.roles]
        if role in roles:
            return True
        return False

    @property
    def is_admin(self):
        if self.has_role('admin'):
            return True
        return False
