from serapeum.lib.db.models import Model
from sqlalchemy import Column, String, Integer, Text


class Role(Model):
    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True)
    name = Column(String(64), index=True)

    def __init__(self, name):
        self.name = name
