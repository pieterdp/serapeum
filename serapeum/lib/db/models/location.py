from serapeum.lib.db.models import Model
from sqlalchemy import Column, String, Integer, Text
from sqlalchemy.orm import relationship


class Location(Model):
    __tablename__ = 'locations'

    id = Column(Integer, primary_key=True)
    room = Column(String(255), index=True)
    rack = Column(String(255), index=True)
    plank = Column(String(255), index=True)
    books = relationship('Book')

    def __init__(self, room, rack, plank):
        self.room = room
        self.rack = rack
        self.plank = plank
