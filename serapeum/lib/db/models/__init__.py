from sqlalchemy.ext.declarative import declarative_base
from serapeum.lib.db import DB

# https://auth0.com/blog/sqlalchemy-orm-tutorial-for-python-developers/


Model = declarative_base()
