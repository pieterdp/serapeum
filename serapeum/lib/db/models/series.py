from serapeum.lib.db.models import Model
from serapeum.lib.db.models.material import Material
from serapeum.lib.db.connection import connection
from sqlalchemy import Column, String, Integer, Text, ForeignKey, Index, func
from sqlalchemy.orm import relationship, backref


class Series(Model):
    __tablename__ = 'series'

    id = Column(Integer, primary_key=True)
    name = Column(Text, index=True, unique=True)
    books = relationship('Book')
    material_id = Column(Integer, ForeignKey('materials.id'))
    material = relationship('Material')

    def __init__(self, name, material, db=None):
        if not db:
            db = connection

        self.name = name
        if not isinstance(material, dict):
            material = {'name': material}
        material_m = db.session.query(Material).filter(Material.name == material['name']).first()
        if not material_m:
            material_m = Material(**material)
            db.session.add(material_m)
        self.material = material_m
