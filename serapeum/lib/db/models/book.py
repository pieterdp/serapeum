from serapeum.lib.db.models import Model
from serapeum.lib.db.models.creator import Creator
from serapeum.lib.db.models.location import Location
from serapeum.lib.db.models.subject import Subject
from serapeum.lib.db.models.series import Series
from serapeum.lib.db.models.material import Material
from serapeum.lib.db.connection import connection
from sqlalchemy import Column, String, Integer, Text, Table, ForeignKey, func, Index
from sqlalchemy.orm import relationship, backref
import re


books_creators_association = Table(
    'books_creators',
    Model.metadata,
    Column('book_id', Integer, ForeignKey('books.id', ondelete='CASCADE', onupdate='CASCADE'), index=True),
    Column('creator_id', Integer, ForeignKey('creators.id', ondelete='CASCADE', onupdate='CASCADE'), index=True)
)


books_subjects_association = Table(
    'books_subjects',
    Model.metadata,
    Column('book_id', Integer, ForeignKey('books.id', ondelete='CASCADE', onupdate='CASCADE'), index=True),
    Column('subject_id', Integer, ForeignKey('subjects.id', ondelete='CASCADE', onupdate='CASCADE'), index=True)
)


class Book(Model):
    __tablename__ = 'books'

    id = Column(Integer, primary_key=True)
    title = Column(Text, index=True)
    creators = relationship('Creator', secondary=books_creators_association, backref='books')
    location_id = Column(Integer, ForeignKey('locations.id'))
    location = relationship('Location')
    subjects = relationship('Subject', secondary=books_subjects_association, backref='books')
    series_id = Column(Integer, ForeignKey('series.id'))
    series = relationship('Series')
    material_id = Column(Integer, ForeignKey('materials.id'))
    material = relationship('Material')
    search = Column(Text)

    __table_args__ = (
        Index(
            'search_tsv',
            func.to_tsvector('dutch', search),
            postgresql_using='gin'
        ),
    )

    def __init__(self, title, creators, location, subjects=(), series=None, material='book', db=None):
        if not db:
            db = connection

        self.title = title

        c = []
        for creator in creators:
            if not isinstance(creator, dict):
                creator = {'name': creator}
            creator_m = db.session.query(Creator).filter(Creator.name == creator['name']).first()
            if not creator_m:
                creator_m = Creator(**creator)
                db.session.add(creator_m)
            c.append(creator_m)
        self.creators = c

        if not isinstance(location, dict):
            location_a = location.split('.')
            if len(location_a) != 3:
                # Some don't have the dots
                r = re.compile('^([A-Z]+)\.?([0-9]+)\.?([0-9]+)$')
                m = r.match(location)
                location = {
                    'room': m.group(1),
                    'rack': m.group(2),
                    'plank': m.group(3)
                }
            else:
                location = {
                    'room': location_a[0],
                    'rack': location_a[1],
                    'plank': location_a[2]
                }
        location_m = db.session.query(Location).filter(
            Location.room == location['room'],
            Location.rack == location['rack'],
            Location.plank == location['plank']
        ).first()
        if not location_m:
            location_m = Location(**location)
            db.session.add(location_m)
        self.location = location_m

        s = []
        for subject in subjects:
            if not isinstance(subject, dict):
                subject = {'subject': subject}
            subject_m = db.session.query(Subject).filter(Subject.subject == subject['subject']).first()
            if not subject_m:
                subject_m = Subject(**subject)
                db.session.add(subject_m)
            s.append(subject_m)
        self.subjects = s

        if not isinstance(material, dict):
            material = {'name': material}
        material_m = db.session.query(Material).filter(Material.name == material['name']).first()
        if not material_m:
            material_m = Material(**material)
            db.session.add(material_m)
        self.material = material_m

        if series:
            if not isinstance(series, dict):
                series = {'name': series, 'material': self.material.name}
            series_m = db.session.query(Series).filter(Series.name == series['name'],
                                                       Series.material == self.material).first()
            if not series_m:
                series_m = Series(**series, db=db)
                db.session.add(series_m)
            self.series = series_m

        self.update_search()

    def update_search(self):
        search = [self.title]
        for creator in self.creators:
            search.append(creator.name)
        for subject in self.subjects:
            search.append(subject.subject)
        if self.series:
            search.append(self.series.name)
        self.search = ' '.join(search)
