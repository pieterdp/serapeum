from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from serapeum.lib.config import Config


class DB:

    def __init__(self):
        c = Config()
        url = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
            c.get('DB_USER'),
            c.get('DB_PASSWORD'),
            c.get('DB_HOST'),
            c.get('DB_PORT'),
            c.get('DB_NAME')
        )
        self.engine = create_engine(
            url
        )
        self._session = None

    @property
    def session(self):
        if not self._session:
            _Session = scoped_session(sessionmaker(bind=self.engine))
            self._session = _Session()
        return self._session

    def create(self, model):
        self.session.add(model)
        self.session.commit()
