from serapeum.lib.api import Api
from serapeum.lib.exceptions import ItemNotFoundException
from serapeum.lib.config import Config
from pymongo import MongoClient, DESCENDING, ASCENDING
from bson import ObjectId
from bson.errors import InvalidId
import urllib.parse
import abc
from flask_babel import gettext as _


class MongoApi(Api):

    def __init__(self, *args, host=None, port=None, username=None, password=None, auth_database=None, **kwargs):
        super(MongoApi, self).__init__(*args, **kwargs)
        c = Config()
        if not host:
            host = c.get('MONGO_SERVER')
        if not port:
            port = c.get('MONGO_PORT')
        if not username:
            username = c.get('MONGO_USER')
        if not password:
            password = c.get('MONGO_PASSWORD')
        if not auth_database:
            auth_database = c.get('MONGO_AUTHDATABASE')
        con_string = 'mongodb://{0}:{1}@{2}:{3}/{4}'.format(
            urllib.parse.quote_plus(username),
            urllib.parse.quote_plus(password),
            host,
            port,
            auth_database
        )
        client = MongoClient(con_string)
        self.db = client['serapeum']

    @abc.abstractmethod
    def bootstrap(self):
        pass

    def add_index(self, name, key=None, params=None):
        if not key:
            key = name
        if not params:
            params = {}
        if name not in self.a.index_information():
            self.a.create_index(key, **params)
        else:
            self.a.drop_index(name)
            self.a.create_index(key, **params)

    def text_search(self, query):
        cursor = self.a.find({'$text': {'$search': query}})
        items_m = list(cursor)
        items = []
        series_a = self.db['series']

        for item in items_m:
            if 'dcterms:isPartOf' in item and item['dcterms:isPartOf'] is not None:
                try:
                    if not isinstance(item['dcterms:isPartOf'], ObjectId):
                        item['dcterms:isPartOf'] = ObjectId(item['dcterms:isPartOf'])
                    item['dcterms:isPartOf'] = series_a.find_one({'_id': item['dcterms:isPartOf']})
                except InvalidId:
                    pass
            items.append(item)

        if len(items) == 0:
            raise ItemNotFoundException(_('Query "%(query)s" heeft geen resultaten opgeleverd.', query=query))
        return items

    def query(self, query, raw=False):
        cursor = self.a.find(query)
        if raw:
            return cursor
        items_m = list(cursor)
        items = []
        series_a = self.db['series']

        for item in items_m:
            if 'dcterms:isPartOf' in item and item['dcterms:isPartOf'] is not None:
                try:
                    if not isinstance(item['dcterms:isPartOf'], ObjectId):
                        item['dcterms:isPartOf'] = ObjectId(item['dcterms:isPartOf'])
                    item['dcterms:isPartOf'] = series_a.find_one({'_id': item['dcterms:isPartOf']})
                except InvalidId:
                    pass
            items.append(item)

        if len(items) == 0:
            raise ItemNotFoundException(_('Query "%(query)s" heeft geen resultaten opgeleverd.', query=query))
        return items

    def search_paginated(self, query, last_id=None, page_size=20, first_id=None):
        if last_id and not isinstance(last_id, ObjectId):
            last_id = ObjectId(last_id)
        if first_id and not isinstance(first_id, ObjectId):
            first_id = ObjectId(first_id)
        total = self.a.find(query).count()
        series_a = self.db['series']
        if last_id:
            cursor = self.__search_from_id(query, last_id, page_size)
            results_m = list(cursor)
        elif first_id:
            cursor = self.__search_to_id(query, first_id, page_size)
            results_m = list(cursor)
            results_m.reverse()
        else:
            cursor = self.a.find(query).limit(page_size)
            results_m = list(cursor)
        remaining_c = cursor.clone()
        remaining = remaining_c.count()
        
        results_a = []

        for result_m in results_m:
            if 'dcterms:isPartOf' in result_m and result_m['dcterms:isPartOf'] is not None:
                try:
                    if not isinstance(result_m['dcterms:isPartOf'], ObjectId):
                        result_m['dcterms:isPartOf'] = ObjectId(result_m['dcterms:isPartOf'])
                    result_m['dcterms:isPartOf'] = series_a.find_one({'_id': result_m['dcterms:isPartOf']})
                except InvalidId:
                    pass
            results_a.append(result_m)

        result = {
            'remaining': remaining,
            'total': total,
            'results': results_a
        }
        if len(results_a) > 0:
            result['last_id'] = results_a[-1]['_id']
            result['first_id'] = results_a[0]['_id']
        return result

    def __search_to_id(self, query, first_id, page_size):
        if not isinstance(first_id, ObjectId):
            first_id = ObjectId(first_id)
        query['_id'] = {'$lt': first_id}
        cursor = self.a.find(query).sort([('_id', DESCENDING)]).limit(page_size)
        # Do not use cursor.sort(ASCENDING) as that will give you the first 20 results
        # Only the last sort() applied to this cursor has any effect.
        # (http://api.mongodb.com/python/current/api/pymongo/cursor.html)
        return cursor

    def __search_from_id(self, query, last_id, page_size):
        if not isinstance(last_id, ObjectId):
            last_id = ObjectId(last_id)
        query['_id'] = {'$gt': last_id}
        cursor = self.a.find(query).limit(page_size)
        return cursor
