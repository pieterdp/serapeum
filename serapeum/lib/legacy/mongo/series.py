from serapeum.lib.legacy.mongo import MongoApi
from bson import ObjectId
from serapeum.lib.exceptions import ItemNotFoundException
from pymongo import TEXT, ASCENDING
from flask_babel import gettext as _


class MongoSeriesApi(MongoApi):
    required = ['name']
    valid = ['creator', 'book_ids', 'type']

    def __init__(self, *args, **kwargs):
        super(MongoSeriesApi, self).__init__(*args, **kwargs)
        self.a = self.db['series']
        self.bootstrap()

    def bootstrap(self):
        self.add_index(
            'series',
            [
                ('dc:title', TEXT)
            ],
            {'name': 'series'}
        )
        self.add_index(
            'series',
            [
                ('dc:title', ASCENDING)
            ],
            {'name': 'series_a'}
        )
        self.add_index(
            'series',
            [
                ('dcterms:hasPart', ASCENDING)
            ],
            {'name': 'items_a'}
        )
        self.add_index(
            'series',
            [
                ('dc:type', ASCENDING)
            ],
            {'name': 'series_type'}
        )

    def create(self, input_data, **kwargs):
        self.check_input_data(input_data)
        series = {
            'dc:title': input_data['name'],
            'dc:creator': None,
            'dcterms:hasPart': [],
            'dc:type': 'books'
        }
        if 'creator' in input_data:
            series['dc:creator'] = input_data['creator']
        if 'type' in input_data:
            series['dc:type'] = input_data['type']
        if 'book_ids' in input_data:
            for book_id in input_data['book_ids']:
                if not isinstance(book_id, ObjectId):
                    book_id = ObjectId(book_id)
                series['dcterms:hasPart'].append(book_id)
        return self.read(self.a.insert_one(series).inserted_id)

    def read(self, item_id, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        item = self.a.find_one({'_id': item_id})
        if not item:
            raise ItemNotFoundException(_('Er bestaat geen serie met ID %(item_id)s.', item_id=item_id))
        return item

    def update(self, item_id, input_data, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        updates = {}
        if 'name' in input_data:
            updates['dc:title'] = input_data['name']
        if 'creator' in input_data:
            updates['dc:creator'] = input_data['creator']
        if 'type' in input_data:
            updates['dc:type'] = input_data['type']
        if 'book_ids' in input_data:
            updates['dcterms:hasPart'] = []
            for book_id in input_data['book_ids']:
                if not isinstance(book_id, ObjectId):
                    book_id = ObjectId(book_id)
                updates['dcterms:hasPart'].append(book_id)
        self.a.update_one({'_id': item_id}, {'$set': updates})
        return self.read(item_id)

    def delete(self, item_id, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        return self.a.delete_one({'_id': item_id})

    def list(self, **kwargs):
        return self.a.find({})

    def add_book_to_series(self, series_id, book_id):
        if not isinstance(series_id, ObjectId):
            series_id = ObjectId(series_id)
        if not isinstance(book_id, ObjectId):
            book_id = ObjectId(book_id)
        series = self.read(series_id)
        if 'dcterms:hasPart' not in series:
            updates = {
                'book_ids': [book_id]
            }
            return self.update(series_id, updates)
        else:
            if book_id not in series['dcterms:hasPart']:
                updates = {
                    'book_ids': series['dcterms:hasPart']
                }
                updates['book_ids'].append(book_id)
                return self.update(series_id, updates)
        return self.read(series_id)

    def remove_book_from_series(self, series_id, book_id):
        if not isinstance(series_id, ObjectId):
            series_id = ObjectId(series_id)
        if not isinstance(book_id, ObjectId):
            book_id = ObjectId(book_id)
        series = self.read(series_id)
        if 'dcterms:hasPart' not in series:
            return self.read(series_id)
        if book_id not in series['dcterms:hasPart']:
            return self.read(series_id)
        else:
            updates = {
                'book_ids': series['dcterms:hasPart']
            }
            updates['book_ids'].remove(book_id)
            return self.update(series_id, updates)

    def by_title(self, series_title):
        items = self.a.find({'dc:title': series_title})
        if not items or items.count() == 0:
            raise ItemNotFoundException('No series called {0}.'.format(series_title))
        return items

    def by_title_s(self, series_title):
        item = self.a.find({'dc:title': series_title})
        if not item or item.count() == 0:
            raise ItemNotFoundException('No series called {0}.'.format(series_title))
        return item[0]

    def by_author_and_title_s(self, series_title, series_author):
        item = self.a.find({'dc:title': series_title, 'dc:creator': series_author})
        if not item or item.count() == 0:
            raise ItemNotFoundException('No series called {0} with author {0}.'.format(series_title, series_author))
        return item[0]

    def by_series_type(self, series_type):
        return list(self.a.find({'dc:type': series_type}))
