from serapeum.lib.legacy.mongo import MongoApi
from bson import ObjectId
from serapeum.lib.exceptions import ItemNotFoundException
from serapeum.lib.legacy.mongo.location import MongoLocationApi
from serapeum.lib.legacy.mongo.series import MongoSeriesApi
from pymongo import TEXT, ASCENDING, DESCENDING
from flask_babel import gettext as _


class MongoBookApi(MongoApi):
    required = ['title', 'creator', 'location']
    valid = ['type', 'subjects', 'identifier']

    def __init__(self, *args, **kwargs):
        super(MongoBookApi, self).__init__(*args, **kwargs)
        self.a = self.db['books']
        self.l = MongoLocationApi()
        self.series_a = MongoSeriesApi()

    def bootstrap(self):
        self.add_index(
            'fulltext',
            [
                ('dc:creator', TEXT),
                ('dc:title', TEXT),
                ('dc:source', TEXT),
                ('dc:subject', TEXT),
                ('_series_t', TEXT)
            ],
            {'name': 'fulltext'}
        )
        self.add_index(
            'location',
            [
                ('dc:source', ASCENDING)
            ],
            {'name': 'location'}
        )
        self.add_index(
            'series',
            [
                ('dcterms:isPartOf', ASCENDING)
            ],
            {'name': 'series'}
        )
        self.add_index(
            'subjects',
            [
                ('dc:subject', ASCENDING)
            ],
            {'name': 'subjects'}
        )
        self.add_index(
            'types',
            [
                ('dc:type', ASCENDING)
            ],
            {'name': 'types'}
        )

    def create(self, input_data, **kwargs):
        self.check_input_data(input_data)
        if not isinstance(input_data['creator'], list):
            author = [input_data['creator']]
        else:
            author = input_data['creator']
        book = {
            'dc:creator': author,
            'dc:title': input_data['title'],
            'dc:identifier': input_data['identifier'] if 'identifier' in input_data else None,
            'dc:source': input_data['location'],
            'dc:subject': [],
            'dc:type': 'book'
        }
        if 'subjects' in input_data:
            if not isinstance(input_data['subjects'], list):
                input_data['subjects'] = list(input_data['subjects'])
            book['dc:subject'] = input_data['subjects']
        if 'type' in input_data:
            book['dc:type'] = input_data['type']
        if 'series' in input_data:
            if not isinstance(input_data['series'], ObjectId):
                input_data['series'] = ObjectId(input_data['series'])
            series = self.series_a.read(input_data['series'])
            book['_series_t'] = series['dc:title']
            book['dcterms:isPartOf'] = input_data['series']
        inserted = self.a.insert_one(book)
        return self.read(inserted.inserted_id)

    def read(self, item_id, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        item = self.a.find_one({'_id': item_id})
        if not item:
            raise ItemNotFoundException(_('Er bestaat geen boek met ID %(item_id)s.', item_id=item_id))
        if 'dcterms:isPartOf' in item\
                and item['dcterms:isPartOf'] is not None\
                and item['dcterms:isPartOf'] != '':
            series = self.series_a.read(item['dcterms:isPartOf'])
            item['dcterms:isPartOf'] = series
        return item

    def update(self, item_id, input_data, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        updates = {}
        if 'creator' in input_data:
            if not isinstance(input_data['creator'], list):
                updates['dc:creator'] = [input_data['creator']]
            else:
                updates['dc:creator'] = input_data['creator']
        if 'title' in input_data:
            updates['dc:title'] = input_data['title']
        if 'identifier' in input_data:
            updates['dc:identifier'] = input_data['identifier']
        if 'location' in input_data:
            updates['dc:source'] = input_data['location']
        if 'type' in input_data:
            updates['dc:type'] = input_data['type']
        if 'series' in input_data:
            if not isinstance(input_data['series'], ObjectId):
                input_data['series'] = ObjectId(input_data['series'])
            series = self.series_a.read(input_data['series'])
            updates['_series_t'] = series['dc:title']
            updates['dcterms:isPartOf'] = input_data['series']
        if 'subjects' in input_data:
            updates['dc:subject'] = input_data['subjects']
        self.a.update_one({'_id': item_id}, {'$set': updates})
        return self.read(item_id)

    def delete(self, item_id, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        return self.a.delete_one({'_id': item_id})

    def list(self, **kwargs):
        books_m = self.a.find({})
        books = []
        for book_m in books_m:
            if 'dcterms:isPartOf' in book_m\
                    and book_m['dcterms:isPartOf'] is not None\
                    and book_m['dcterms:isPartOf'] != '':
                book_m['dcterms:isPartOf'] = self.series_a.read(book_m['dcterms:isPartOf'])
            books.append(book_m)
        return books

    def by_identifier(self, identifier):
        item = self.a.find_one({'dc:identifier': identifier})
        if not item:
            raise ItemNotFoundException(_('Er bestaat geen boek met ID %(item_id)s.', item_id=identifier))
        return item

    def by_location(self, location, last_id=None, first_id=None):
        return self.search_paginated({'dc:source': location}, first_id=first_id, last_id=last_id)

    def in_series(self):
        return self.a.find({'dcterms:isPartOf': {'$ne': None}})

    def by_type(self, book_type):
        return list(self.a.find({'dc:type': book_type}))

    def by_title(self, title):
        book = self.a.find({'dc:title': title})
        if not book or book.count() == 0:
            raise ItemNotFoundException('No book with title {0}.'.format(title))
        return book[0]
