from serapeum.lib.legacy.mongo import MongoApi
from bson import ObjectId
from serapeum.lib.exceptions import ItemNotFoundException
from pymongo import TEXT, ASCENDING
from flask_babel import gettext as _


class MongoLocationApi(MongoApi):
    required = ['code', 'b_case', 'plank']

    def __init__(self, *args, **kwargs):
        super(MongoLocationApi, self).__init__(*args, **kwargs)
        self.a = self.db['locations']

    def bootstrap(self):
        self.add_index(
            'fulltext',
            [
                ('code', TEXT),
                ('b_case', TEXT),
                ('plank', TEXT)
            ],
            {'name': 'fulltext'}
        )
        self.add_index(
            'code',
            [
                ('code', ASCENDING)
            ],
            {'name': 'code'}
        )
        self.add_index(
            'b_case',
            [
                ('b_case', ASCENDING)
            ],
            {'name': 'b_case'}
        )
        self.add_index(
            'plank',
            [
                ('plank', ASCENDING)
            ],
            {'name': 'plank'}
        )

    def create(self, input_data, **kwargs):
        self.check_input_data(input_data)
        location = {
            'code': input_data['code'],
            'case': input_data['b_case'],
            'plank': input_data['plank'],
            'location': self.mk_location(input_data['code'], input_data['b_case'], input_data['plank'])
        }
        return self.a.insert_one(location)

    def read(self, item_id, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        item = self.a.find_one({'_id': item_id})
        if not item:
            raise ItemNotFoundException(_('Er bestaat geen locatie met ID %(item_id)s.', item_id=item_id))
        return item

    def update(self, item_id, input_data, **kwargs):
        pass

    def delete(self, item_id, **kwargs):
        if not isinstance(item_id, ObjectId):
            item_id = ObjectId(item_id)
        return self.a.delete_one({'_id': item_id})

    def list(self, **kwargs):
        return self.a.find({})

    def all_codes(self):
        return self.a.find({}).distinct('code')

    def all_cases(self, code):
        return self.a.find({'code': code}).distinct('case')

    def all_planks(self, code, case):
        planks = self.a.find({'code': code, 'case': str(case)}).distinct('plank')
        if not planks or len(planks) == 0:
            return self.a.find({'code': code, 'case': int(case)}).distinct('plank')
        else:
            return planks

    def mk_location(self, code, case, plank):
        return '{0}.{1}.{2}'.format(code, case, plank)
